trigger PopulateOrderStaus on Order (Before Insert, Before Update) 
{
    if (TriggerDisabler__c.getInstance().OrderItem_OrderItem__c) 
    {
        return;
    }
    
    if(Trigger.isInsert || Trigger.isUpdate)
    {
        PopulateOrderStatus.updateOrderStatusValue(Trigger.new);
        
    }

}
trigger UpdateOrderNumberToOrder on OrderItem (After Insert, After update, Before Update) 
{
    if (TriggerDisabler__c.getInstance().OrderItem_OrderItem__c) 
    {
        return;
    }
    
    if(Trigger.isupdate && Trigger.isBefore)
    {
         list<OrderItem> orderItemInsert = [select ID, OrderId,DummyChild__c from OrderItem where ID IN :trigger.new];
           
           for(OrderItem oitem : trigger.new)
           {
               oitem.DummyChild__c = 'child';
               
           }
             
    }
    
    if(Trigger.isUpdate && Trigger.isAfter)
    {
          List<id> OrderParentID = new List<id>();                 
        
            for(OrderItem oItemObj : trigger.new)
            {
                OrderParentID.add(oItemObj.OrderID);           
                
                system.debug('Order ID ==> ' +OrderParentID);  // Account ID ==> 801g0000002jFA3AAM
            }
            
            List<OrderItem> orderValues = [SELECT Id, OrderID,Pricebookentry.ProductCode,Quantity,UnitPrice,DummyChild__c FROM orderItem where OrderID IN:OrderParentID ];
            system.debug('order Values ==> ' +orderValues);
            
            String productCode;
            for(OrderItem ot : orderValues)
            {
                productCode = ot.Pricebookentry.ProductCode;
                
            }            
           
          
                    
        List<order> updateToOrder = [ SELECT id , name , ShippingPostalCode, ShippingState, shippingCountry, shippingStreet,shippingCity, BillingStreet,BillingCity,BillingCountry, BillingState, BillingPostalCode,  updateFromOrderItem__c,DummyParent__c FROM order WHERE id=:OrderParentID ];
        // List<order> updateToOrder = [ SELECT id , name, shippingStreet,  updateFromOrderItem__c FROM order WHERE id=:OrderParentID ];
        for(order o : updateToOrder)
        {
            o.updateFromOrderItem__c = productCode; 
            o.BillingStreet = o.shippingStreet;
            o.BillingCity = o.shippingCity;
            o.BillingCountry = o.shippingCountry;
            o.BillingState = o.ShippingState;
            o.BillingPostalCode = o.ShippingPostalCode;
            o.DummyParent__c = 'parent';
        }
        
        update updateToOrder;
        
    }
}
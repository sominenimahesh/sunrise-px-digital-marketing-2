/** * 
* File Name: GiftBusinessRulesEligibilityTrigger
* Description : Trigger is being developed to execute Gift business rules based on customer eligibility.
* Copyright : Johnson & Johnson
* * @author : Mahesh Somineni

* Modification Log =============================================================== 
*     Ver  |Date       |Author         |Modification
*    1.0  |30/07/2015  |@msominen@its.jnj.com  |New Trigger created
* */ 
trigger GiftBusinessRulesEligibilityTrigger on Device__c (after insert) {

    //if(checkRecursive.runOnce())
    //{
      GiftBusinessRulesUSService GiftRules = new GiftBusinessRulesUSService();
      GiftRules.GiftEligibility(Trigger.new);
    //}

}
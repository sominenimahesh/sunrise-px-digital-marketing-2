global class RedemptionNoChargemeter implements Database.Batchable<sObject>,Database.Stateful 
{
     global final String query;
     global final String noChargemetr;
    
    List<RedemptionTempTable__c> deleteProcessedTempCustomers = new List<RedemptionTempTable__c>();
       
    global RedemptionNoChargemeter()
    {  
       system.debug('inside constructor');
        
       noChargemetr= 'NOCHARGEMETR';
       query='select id , CardIDNumber__c,DrugName__c,GroupNumber__c,CopayAmount__c,PatientAge__c,PharmacyNABP__c,ClaimType__c,MailOrderIndicator__c,PatientGender__c,OtherCoverageCode__c,DEA__c,VoucherReceivedDate__c,RXOrigin__c,NDC__c,TotalAmountPaid__c,HCP__c From RedemptionTempTable__c where CardIDNumber__c=:noChargemetr';
       
       system.debug('redemption ==>'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<RedemptionTempTable__c> ListofTempData = (List<RedemptionTempTable__c>)scope;
        
        system.debug('ListofTempData ==> ' +ListofTempData );
        system.debug('ListofTempData size ==> ' +ListofTempData.size() );
        
        List<RedemptionTempTable__c> deleteAllRedemptionData = [ Select id , Name From RedemptionTempTable__c ];   
        for(RedemptionTempTable__c delRt : deleteAllRedemptionData)
         {
           deleteProcessedTempCustomers.add(delRt);
         }   
               
         List<Promotion_Transaction__c> newVoucher = new List<Promotion_Transaction__c>();
         
         List<String> NABPnumbers = new List<String>();
         List<String> GroupNumbers =  new List<String>();
         List<String> HCPnumbers = new List<String>();
         for(RedemptionTempTable__c rt : ListofTempData)
         {
           GroupNumbers.add(rt.GroupNumber__c );
           NABPnumbers.add(rt.PharmacyNABP__c);
           HCPnumbers.add(rt.HCP__c);
         }   
         
         // Promotion Item Group ID Values
         Map<String,Promotion_Item__c> mapListGroup  = new Map<String,Promotion_Item__c>();    
         List<Promotion_Item__c> ListGroup = [ Select id , Legacy_Record_Id__c From Promotion_Item__c where Legacy_Record_Id__c IN:GroupNumbers ];   
         for(Promotion_Item__c groupID : ListGroup)
         {
             mapListGroup.put(groupID.Legacy_Record_Id__c,groupID);
         }
         
         // Pharmacy NABP Values  
         Map<String,Pharmacy__c> mapPharmacy = new Map<String,Pharmacy__c>();
         List<Pharmacy__c> listPharmacy = [select id , NABP__c , Name from Pharmacy__c where NABP__c IN:NABPnumbers];
         for(Pharmacy__c pharmacyValues : listPharmacy)
         {
              mapPharmacy.put(pharmacyValues.NABP__c,pharmacyValues);
         }
         
         // HCP for Business contacts
         Map<String,Contact> mapHCP = new Map<String,Contact>();
         List<Contact> listOfHCPNum = [select id , HCP__c , Name from Contact where HCP__c IN:HCPnumbers];
         for(Contact conHCPValues : listOfHCPNum)
         {
              mapHCP.put(conHCPValues.HCP__c,conHCPValues);
         }
         
           
        for(RedemptionTempTable__c eachRedemptionData : ListofTempData)
        {
            
              system.debug('ListGroup ==> ' +ListGroup );
              
              
               if(!ListGroup.isEmpty())
               {
                   
                       //system.debug('id is'+ListGroup[0]);
                       system.debug('mapListGroup is==>'+mapListGroup);
                       
                       Pharmacy__c pharmacyRowID = mapPharmacy.get(eachRedemptionData.PharmacyNABP__c);  
                       Contact contactRowID = mapHCP.get(eachRedemptionData.HCP__c);  
                       Promotion_Item__c GroupRowID = mapListGroup.get(eachRedemptionData.GroupNumber__c);  
                       system.debug('GroupRowID  is==>'+GroupRowID ); 
                                 
                       Promotion_Transaction__c createVoucher = new Promotion_Transaction__c();
                                
                       //createVoucher.Promotion_Item__c =ListGroup[0].id;
                       createVoucher.Promotion_Item__c = GroupRowID.id;
                       createVoucher.Pharmacy__c = pharmacyRowID.id;
                       createVoucher.HCP__c = contactRowID.id;
                       createVoucher.VoucherNumber__c = 'No Voucher';
                       createVoucher.GroupNumber__c = eachRedemptionData.GroupNumber__c;
                       createVoucher.DrugName__c =  eachRedemptionData.DrugName__c;
                       createVoucher.PatientAge__c =  eachRedemptionData.PatientAge__c;
                       createVoucher.CopayAmount__c =  eachRedemptionData.CopayAmount__c;
                       createVoucher.PatientGenderPickllist__c =  eachRedemptionData.PatientGender__c;
                       createVoucher.ClaimTypePicklist__c = eachRedemptionData.ClaimType__c;
                       createVoucher.MailOrderIndicatorPickList__c = eachRedemptionData.MailOrderIndicator__c;
                       createVoucher.OtherCoverageCodePickList__c = eachRedemptionData.OtherCoverageCode__c;
                       createVoucher.VoucherReceivedDate__c= eachRedemptionData.VoucherReceivedDate__c;
                       createVoucher.DEA__c = eachRedemptionData.DEA__c;
                       createVoucher.Status__c = 'Redeemed';
                       createVoucher.RXOrigin__c= eachRedemptionData.RXOrigin__c;
                       createVoucher.NDC__c= eachRedemptionData.NDC__c;
                       createVoucher.TotalAmountPaid__c= eachRedemptionData.TotalAmountPaid__c;
                       newVoucher.add(createVoucher);
      
               
                  system.debug('newVoucher==> ' +newVoucher);
               }
               
          //  deleteProcessedTempCustomers.add(eachRedemptionData);
          //  system.debug('deleteProcessedTempCustomers==> ' +deleteProcessedTempCustomers );
           // deleteProcessedTempCustomers.add(deleteAllRedemptionData);
        }
         
        if(!newVoucher.isEmpty())
        {
           insert newVoucher;
        }
        
        
   }
   
   global void finish(Database.BatchableContext BC)
   {
       if(!deleteProcessedTempCustomers.isempty())
        {
           delete deleteProcessedTempCustomers;
        }
            
   }
  
}
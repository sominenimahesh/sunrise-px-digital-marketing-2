global class CustomBrandshop implements Database.Batchable<sObject>,Database.Stateful 
{
    global final String query;
    global final String BRANDSHOP;
    
    List<CustomerTempTable__c> deleteProcessedTempCustomers = new List<CustomerTempTable__c>();
       
    global CustomBrandshop()
    {  
      
       
      BRANDSHOP = 'BRANDSHOP';
     query='select id,FirstName__c,LastName__c,Diabetes__c,SerialNumber__c,Title__c,Suffix__c,MeterMostOftenUsed__c,Relationship__c,OftenTesting__c,RegisteredDate__c,ManageMethods__c,Mobile__c,HomePhone__c,Age__c, Email__c,DirectMailPrivacy__c,EmailPrivacy__c,PhonePrivacy__c,SMSPrivacy__c, Address__c, Country__c, PrimaryCity__c, PrimaryStateProvince__c, PrimaryStreet__c, PrimaryZipPostalCode__c,Gender__c,TypeOfFile__c  From CustomerTempTable__c where TypeOfFile__c=:BRANDSHOP';
       
       
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<CustomerTempTable__c> ListofTempCustomer = (List<CustomerTempTable__c>)scope;
         RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' ]; 
      
          String Fname ;
          String UFname ;
           
         
          String Lname;
          String ULname;
        
         List<Account> insertAccounts =new List<Account>();       
         List<Account> updateAccounts =new List<Account>();
          List<productsSelfReported__c> updateselfproduct=new List<productsSelfReported__c>();
         List<ProductsSelfReported__c> newProductsSelf =new List<ProductsSelfReported__c>();
            
     
     
      List<ProductsSelfReported__c> Oldselfproduct = [Select Account__c,id,RegisteredDate__c,Product__c from ProductsSelfReported__c LIMIT 50000];   
         Map<Id,List<ProductsSelfReported__c>> OldselfreportedMap = new Map<id,List<ProductsSelfReported__c>>();
            

        for(ProductsSelfReported__c de : Oldselfproduct) {
            if(OldselfreportedMap.containsKey(de.Account__c)) {
            List<ProductsSelfReported__c> oself= OldselfreportedMap.get(de.Account__c);
             oself.add(de);
            OldselfreportedMap.put(de.Account__c, oself);
             } else {
             OldselfreportedMap.put(de.Account__c, new List<ProductsSelfReported__c> { de });
            }
        }
  
  
 // Mapping Elitem Num for self Reported Product
        
         List<String> ELtemNo =  new List<String>();
         for(CustomerTempTable__c ct : ListofTempCustomer)
         {
           ELtemNo.add(ct.SerialNumber__c);
         }   
             
           Map<String,Product2> mapProduct = new Map<String,Product2>();
           List<Product2> allElitemNum = [select id , EltemNo__c, Name from Product2 where EltemNo__c IN:ELtemNo];
           for(Product2 productElValues : allElitemNum)
           {
               mapProduct.put(productElValues.EltemNo__c,productElValues);
           }
           
   // US states Code
   
       Map<string,USStatesCode__c> mapUSStatesCodes = USStatesCode__c.getAll();     // Custom Setting Object  'USStatesCode__c'
        
       for(CustomerTempTable__c eachCustomerTempRec : ListofTempCustomer)
        {
      
             Fname = eachCustomerTempRec.FirstName__c;
             UFname = Fname.toUpperCase();
             
             system.debug('UFname ==>'+UFname );
             
             Lname = eachCustomerTempRec.LastName__c;
             ULname = Lname.toUpperCase();
             system.debug('address ==>'+eachCustomerTempRec.Address__c);
             
              List<Account> ListAccount =[Select id,FirstName,LastName,PersonHomePhone,PersonEmail
            From Account where FirstName =:UFname AND LastName =:ULname AND PersonEmail=:eachCustomerTempRec.Email__c];  
            
             
             system.debug('ListAccount==>'+ListAccount);
              CustomerTempTable__c c = new CustomerTempTable__c();
              
              if(listAccount.size()> 0)
               {
                 Boolean ProductsSelfReported = false;
                 System.debug('entered if loop');
                  Account ac = new Account();
                 ac.Id = ListAccount[0].id;
                 ac.Direct_Mail_Privacy__pc =eachCustomerTempRec.DirectMailPrivacy__c;
                 ac.Email_Privacy__pc =eachCustomerTempRec.EmailPrivacy__c;
                 ac.Phone_Privacy__pc =eachCustomerTempRec.PhonePrivacy__c;
                 ac.SMS_Privacy__pc =eachCustomerTempRec.SMSPrivacy__c;
                 ac.PersonEmail=eachCustomerTempRec.Email__c;
                 ac.LastName=eachCustomerTempRec.LastName__c;
                 ac.FirstName=eachCustomerTempRec.FirstName__c;
                 ac.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 //ac.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 ac.ShippingState=mapUSStatesCodes.get(eachCustomerTempRec.PrimaryStateProvince__c).USStatesName__c;
                 ac.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 ac.ShippingCountry='USA';
                 ac.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 ac.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                ac.MeterRegisteredDate__c =eachCustomerTempRec.RegisteredDate__c;
                 ac.Age__c = eachCustomerTempRec.Age__c;
                 ac.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 ac.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 ac.Relationship__c=eachCustomerTempRec.Relationship__c;
                 ac.PersonTitle=eachCustomerTempRec.Title__c;
                 ac.Suffix=eachCustomerTempRec.Suffix__c;
                 ac.RecordTypeId = rt.id;
                 ac.Email_Permission_Source__pc='Brandshop';
                 ac.Direct_Mail_Permission_Source__pc='Brandshop';
                 ac.SMS_Permission_Source__pc='Brandshop';
                 ac.Phone_Permission_Source__pc='Brandshop';
                 
              
                 ac.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
                 ac.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                
                if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    ac.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    ac.Phone_Privacy__pc='LFS';
                  }
                  
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    ac.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    ac.SMS_Privacy__pc='LFS';
                  }
                     if(eachCustomerTempRec.Gender__c=='M')
                  {
                    ac.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    ac.Gender__pc='Female';
                  }
                   updateAccounts.add(ac);
                 
                deleteProcessedTempCustomers.add(eachCustomerTempRec);
                  
                
                  List<productsSelfReported__c> oproduct = OldselfreportedMap.get(ListAccount[0].id);
                   if(oproduct!=null)
                    {
                   for(productsSelfReported__c d: oproduct)
                   {
                       Product2 productRowID = mapProduct.get(eachCustomerTempRec.SerialNumber__c);
                       if(d.Product__c==productRowID.Id)
                       {
                           d.RegisteredDate__c=ac.MeterRegisteredDate__c;
                           ProductsSelfReported = true;
                           updateselfproduct.add(d);
                            
                       }
                         
                   }
                    
                    
                   }
                   if(ProductsSelfReported==false)
                   {
                      Product2 productRowID = mapProduct.get(ac.SerialNumber__c);
                      ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                        
                     newProductsSelfReported.Account__c = ac.id;
                    newProductsSelfReported.Product__c =  productRowID.Id;
                    newProductsSelfReported.RegisteredDate__c =ac.MeterRegisteredDate__c;   
                    newProductsSelf.add(newProductsSelfReported);
                   
                   // }
                     //}
                    }
                   }                
               else
                  {
                  
                      System.debug('entered else loop');
 //********************************************************************************patient US *****************************************************************                                 
                   //if(listAccount.isEmpty) 
              // {
              
              
                 try
                     { 
                         //Product2 productRowID = mapProduct.get(eachCustomerTempRec.SerialNumber__c);
                         Account acc = new Account();
                         acc.Direct_Mail_Privacy__pc = eachCustomerTempRec.DirectMailPrivacy__c;
                         acc.Email_Privacy__pc = eachCustomerTempRec.EmailPrivacy__c;
                         acc.Phone_Privacy__pc = eachCustomerTempRec.PhonePrivacy__c;
                         acc.SMS_Privacy__pc = eachCustomerTempRec.SMSPrivacy__c;
                         acc.PersonEmail=eachCustomerTempRec.Email__c;
                         acc.LastName=eachCustomerTempRec.LastName__c;
                         acc.FirstName=eachCustomerTempRec.FirstName__c;
                         System.debug('success'+eachCustomerTempRec.PrimaryStreet__c);
                         acc.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                         //acc.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                         acc.ShippingState=mapUSStatesCodes.get(eachCustomerTempRec.PrimaryStateProvince__c).USStatesName__c;
                         acc.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                         acc.ShippingCountry='USA';
                         acc.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                         acc.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                         acc.Age__c = eachCustomerTempRec.Age__c;
                         acc.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                         acc.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                         acc.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                         acc.Relationship__c=eachCustomerTempRec.Relationship__c;
                         acc.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
                         acc.Email_Permission_Source__pc='Brandshop';
                         acc.Direct_Mail_Permission_Source__pc='Branshop';
                         acc.SMS_Permission_Source__pc='Branshop';
                         acc.Phone_Permission_Source__pc='Branshop';
                         acc.PersonTitle=eachCustomerTempRec.Title__c;
                         acc.Suffix=eachCustomerTempRec.Suffix__c;
                         acc.MeterRegisteredDate__c =eachCustomerTempRec.RegisteredDate__c;
                         acc.RecordTypeId = rt.id;
                         
                        if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                          {
                            acc.Email_Privacy__pc='LFS';
                          }
                            if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                          {
                            acc.Phone_Privacy__pc='LFS';
                          }
                           if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                          {
                            acc.Direct_Mail_Privacy__pc='LFS';
                          }
                            if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                          {
                            acc.SMS_Privacy__pc='LFS';
                          }
                          
                             if(eachCustomerTempRec.Gender__c=='M')
                          {
                            acc.Gender__pc='Male';
                          }
                            if(eachCustomerTempRec.Gender__c=='F')
                          {
                            acc.Gender__pc='Female';
                          }
                         
                          
                          insertAccounts.add(acc);
                
                 }
                 catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }
                      
                 deleteProcessedTempCustomers.add(eachCustomerTempRec);
               //}
               }
           
            
 }
       
    if(!insertAccounts.isEmpty())
       {
           insert insertAccounts;
       }  
   
//*************************************************************selfreported for patient Us***************************************************   
    for(Account acc:insertAccounts)
       {
           system.debug('id is'+acc);
           
      
                 //create ProductsSelfReported
                  Product2 productRowID = mapProduct.get(acc.SerialNumber__c);
                  ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                    
                 newProductsSelfReported.Account__c = acc.id;
                newProductsSelfReported.Product__c =  productRowID.Id;
                newProductsSelfReported.RegisteredDate__c =acc.MeterRegisteredDate__c;   
                newProductsSelf.add(newProductsSelfReported);
 
                     
                    
      }
 //****************************************************************selfreported for patient us end here**********************************************     
          if(!newProductsSelf.isEmpty())
            {
           
            insert newProductsSelf;
            }
           
           
           if(!updateAccounts.isEmpty())
            {
              update updateAccounts;
            }
           if(!updateselfproduct.isEmpty())
            {
              update updateselfproduct;
            }  
          
            
         } 
   
   global void finish(Database.BatchableContext BC)
   {
           if(!deleteProcessedTempCustomers.isempty())
            {
              delete deleteProcessedTempCustomers;
            }
   }
  
}
global class DeDuplication implements Database.Batchable<sObject>,Database.Stateful 
{
    global final String query;
    global final String deceased;
    
    List<CustomerTempTable__c> deleteProcessedTempCustomers = new List<CustomerTempTable__c>();
       
    global DeDuplication()
    {  
      // query='Select id,Name,Primary_Address__c,Primary_Address__r.Location__r.Street__c,Primary_Address__r.Location__r.City__c,Primary_Address__r.Location__r.State__c,Primary_Address__r.Location__r.PostalCode__c,Primary_Address__r.Location__r.Country__c,Full_Shipping_Primary_Address__c , Email from Account ';
       
       deceased = 'Deceased';
       query='select id , FirstName__c,LastName__c, Age__c, Email__c, Mobile__c, Country__c, PrimaryCity__c, PrimaryStateProvince__c, PrimaryStreet__c, PrimaryZipPostalCode__c  From CustomerTempTable__c where TypeOfFile__c=:deceased';
       
       system.debug('customer==>'+query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<CustomerTempTable__c> ListofTempCustomer = (List<CustomerTempTable__c>)scope;
             
              
        for(CustomerTempTable__c eachCustomerTempRec : ListofTempCustomer)
        {
           
              List<Account> ListAccount = [ Select id , PersonEmail From Account where PersonEmail=:eachCustomerTempRec.Email__c ];   
 
              system.debug('ListAccount==>'+ListAccount);
              
              list<Account> updateAccounts = new list<Account>();
              
               if(!listAccount.isEmpty())
               {
                 for(Account acc : ListAccount)
                 {
                    /* Account updateAccount = new Account();
                     updateAccount.Id = acc.id;
                     system.debug('updateID==>' + updateAccount.Id);
                     updateAccount.Confirm_Deceased__pc=true;
                    // updateAccounts.add(updateAccount);
                    updateAccounts.add(updateAccount);*/
                    
                    acc.Confirm_Deceased__pc=true;
                    updateAccounts.add(acc);
                 }
                 
                     
                      
                  
                 /*Account updateAccount = new Account();
                 updateAccount.Id = ListAccount[0].id;
                 updateAccount.Confirm_Deceased__pc=true;
                 updateAccounts.add(updateAccount);*/
                 
                /* for(Account acc : ListAccount)
                 {
                    acc.Confirm_Deceased__pc=true;
                    updateAccounts.add(acc);
                 }
                 system.debug('updateAccounts ==>' +updateAccounts );*/
                 //CustomerTempTable__c c = new CustomerTempTable__c();
                 //c.id=eachCustomerTempRec.id;
                
               }  
               
                if(!updateAccounts.isempty())
                {
                    //system.debug('updateAccounts11 ==>' +updateAccounts );
                    update updateAccounts ;
                    //system.debug('updateAccounts123 ==>' +updateAccounts );
                }
               
                deleteProcessedTempCustomers.add(eachCustomerTempRec);
                system.debug('deleteProcessedTempCustomers==> ' +deleteProcessedTempCustomers );
        }
        
         
            
        
   }
   
   global void finish(Database.BatchableContext BC)
   {
           if(!deleteProcessedTempCustomers.isempty())
            {
              delete deleteProcessedTempCustomers;
            }
   }
  
}
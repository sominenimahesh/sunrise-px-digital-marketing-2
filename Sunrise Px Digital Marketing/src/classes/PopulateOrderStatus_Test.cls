@isTest

public class PopulateOrderStatus_Test
{
    static testmethod void populateOrderStatusValues()
    {
        test.StartTest();
        
      RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' Limit 1];
   Account testAccount = new Account();
   testAccount.LastName = 'Shankar';
   testAccount.personEmail = 'shiva@gmail.com';
   testAccount.shippingstreet = 'Dalal Street';
   testAccount.shippingCity = 'Bangalore';
   testAccount.shippingstate = 'Karnataka';
   testAccount.shippingPostalCode = '560045';
   testAccount.shippingCountry = 'India';
   testAccount.RecordTypeId = rt.id;
   insert testAccount;
        
        RecordType orderRecordType = [select id,Name from RecordType where SobjectType='Order' and Name='Customer Order - US' Limit 1];
       
        order  orderobj = new order();
        orderobj.AccountId = testAccount .id;
        orderobj.EffectiveDate = Date.Today();
        orderobj.Type = 'Loaner';
        orderobj.Status = 'Draft';
        orderobj.Shipment_Method__c = 'External';
        orderobj.OrderItemStatus__c = 'S';
        orderobj.RecordTypeId = orderRecordType.ID;
        insert orderobj;
        
        orderobj.OrderItemStatus__c = 'C';
        update orderobj;
        
         system.assertEquals('C',orderobj.OrderItemStatus__c ,'HURRY ITS WRONG OUTPUT');
         
        test.StopTest();
    }
}
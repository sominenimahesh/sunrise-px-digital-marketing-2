/** * 
* File Name: checkRecursive
* Description : THis class is prevent recursive executon of Triggers.
* Copyright : Johnson & Johnson
* * @author : Manmohan Singh |mmo325@its.jnj.com | manmohan.singh1@cognizant.com

* Modification Log =============================================================== 
* 		Ver  |Date 			|Author 				|Modification
*		1.0  |23/07/2015	|@mmo325@its.jnj.com	|New Class created
* */ 
public Class checkRecursive{
    private static boolean run = true;
    // this method will stop recursive executon.
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
}
global class CustomIVR implements Database.Batchable<sObject>,Database.Stateful 
{
    global final String query;
    global final String IVR;
    
    List<CustomerTempTable__c> deleteProcessedTempCustomers = new List<CustomerTempTable__c>();
       
    global CustomIVR()
    {  
      
       
      IVR = 'IVR';
     query='select id,FirstName__c,LastName__c,SerialNumber__c,Title__c,Suffix__c,MeterMostOftenUsed__c,Relationship__c,OftenTesting__c,RegisteredDate__c,ManageMethods__c,Mobile__c,HomePhone__c,Birthdate__c, Email__c,DirectMailPrivacy__c,EmailPrivacy__c,PhonePrivacy__c,SMSPrivacy__c, Address__c, Country__c, PrimaryCity__c, PrimaryStateProvince__c, PrimaryStreet__c, PrimaryZipPostalCode__c,Gender__c,TypeOfFile__c  From CustomerTempTable__c where TypeOfFile__c=:IVR';
       
       
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<CustomerTempTable__c> ListofTempCustomer = (List<CustomerTempTable__c>)scope;
        
          String Fname ;
          String UFname ;
           
         
          String Lname;
          String ULname;
         
         List<Account> insertAccounts =new List<Account>();       
         List<Account> updateAccounts =new List<Account>();
         List<ProductsSelfReported__c> newProductsSelf =new List<ProductsSelfReported__c>();
         
     
     
      List<ProductsSelfReported__c> Oldselfproduct = [Select Account__c,id,RegisteredDate__c,Product__c from ProductsSelfReported__c LIMIT 50000];   
         Map<Id,List<ProductsSelfReported__c>> OldselfreportedMap = new Map<id,List<ProductsSelfReported__c>>();
            

        for(ProductsSelfReported__c de : Oldselfproduct) {
            if(OldselfreportedMap.containsKey(de.Account__c)) {
            List<ProductsSelfReported__c> oself= OldselfreportedMap.get(de.Account__c);
             oself.add(de);
            OldselfreportedMap.put(de.Account__c, oself);
             } else {
             OldselfreportedMap.put(de.Account__c, new List<ProductsSelfReported__c> { de });
            }
        }
        
       for(CustomerTempTable__c eachCustomerTempRec : ListofTempCustomer)
        {
      
             Fname = eachCustomerTempRec.FirstName__c;
             UFname = Fname.toUpperCase();
             
             system.debug('UFname ==>'+UFname );
             
             Lname = eachCustomerTempRec.LastName__c;
             ULname = Lname.toUpperCase();
             system.debug('address ==>'+eachCustomerTempRec.Address__c);
             
              /*List<Account> ListAccount =[Select id,FirstName,LastName,PersonHomePhone,PersonEmail,
              PersonMobilePhone,Relationship__c,PersonTitle,Suffix,SMS_Privacy__pc,Phone_Privacy__pc,
              Email_Privacy__pc,Direct_Mail_Privacy__pc,Test_Frequency__pc,SerialNumber__c,TypeOfFile__c,ShippingStreet,
              ShippingState,ShippingPostalCode,ShippingCountry,ShippingCity,PersonBirthdate 
              From Account where FirstName =:UFname AND LastName =:ULname AND PersonEmail=:eachCustomerTempRec.Email__c];  */
              
              List<Account> ListAccount =[Select id,FirstName,LastName,PersonEmail
              From Account where FirstName =:UFname AND LastName =:ULname AND PersonEmail=:eachCustomerTempRec.Email__c];
            
             
             system.debug('ListAccount==>'+ListAccount);
              CustomerTempTable__c c = new CustomerTempTable__c();
              
              if(listAccount.size()> 0)
               {
                 Boolean ProductsSelfReported = false;
                 System.debug('entered if loop');
                  Account ac = new Account();
                 ac.Id = ListAccount[0].id;
                 ac.Direct_Mail_Privacy__pc =eachCustomerTempRec.DirectMailPrivacy__c;
                 ac.Email_Privacy__pc =eachCustomerTempRec.EmailPrivacy__c;
                 ac.Phone_Privacy__pc =eachCustomerTempRec.PhonePrivacy__c;
                 ac.SMS_Privacy__pc =eachCustomerTempRec.SMSPrivacy__c;
                 ac.PersonEmail=eachCustomerTempRec.Email__c;
                 ac.LastName=eachCustomerTempRec.LastName__c;
                 ac.FirstName=eachCustomerTempRec.FirstName__c;
                 ac.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 ac.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 ac.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 ac.ShippingCountry='USA';
                 ac.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 ac.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;                
                 ac.PersonBirthdate = eachCustomerTempRec.Birthdate__c;
                 ac.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 ac.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 ac.Relationship__c=eachCustomerTempRec.Relationship__c;
                 ac.PersonTitle=eachCustomerTempRec.Title__c;
                 ac.Suffix=eachCustomerTempRec.Suffix__c;
                // ac.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
                 ac.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
               
 //**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
           String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
                  
                   system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 ac.TreatmentMethod__c = treatMethod ;
                 
//********************************** Managemethods ends here ***************************************************************************
              
                   if(eachCustomerTempRec.Gender__c=='M')
                  {
                    ac.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    ac.Gender__pc='Female';
                  }
                  
                  
                   if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    ac.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    ac.Phone_Privacy__pc='LFS';
                  }
                  
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    ac.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    ac.SMS_Privacy__pc='LFS';
                  }
                  
                   updateAccounts.add(ac);
                  
                  deleteProcessedTempCustomers.add(eachCustomerTempRec);
                  
                 system.debug('Vicky==>'+updateAccounts);
                 
                  //String ID = '01tg00000043JjFAAU';
                  List<productsSelfReported__c> oproduct = OldselfreportedMap.get(ListAccount[0].id);
                   if(oproduct!=null)
                    {
                   for(productsSelfReported__c d: oproduct)
                   {
                       if(d.Product__c==eachCustomerTempRec.SerialNumber__c )
                       {
                           ProductsSelfReported = true;
                       }
                       
                      
                   }
                    }
                   
                   if(ProductsSelfReported==false)
                   {
                 
                   ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                    
                   newProductsSelfReported.Account__c = ListAccount[0].id;
                  // newProductsSelfReported.Product__c = eachCustomerTempRec.SerialNumber__c;
                  newProductsSelfReported.Product__c = '01tg00000043JjFAAU';
                   //newProductsSelfReported.RegisteredDate__c = ac.MeterRegisteredDate__c;                
                   newProductsSelfReported.RegisteredDate__c = system.today();                
                   newProductsSelf.add(newProductsSelfReported);
                   
                   }
                    
                 }                
               else
                  {
                  
                      System.debug('entered else loop');
                                  
                   //if(listAccount.isEmpty) 
              // {
                try{ Account acc = new Account();
                 
                 acc.Direct_Mail_Privacy__pc = eachCustomerTempRec.DirectMailPrivacy__c;
                 acc.Email_Privacy__pc = eachCustomerTempRec.EmailPrivacy__c;
                 acc.Phone_Privacy__pc = eachCustomerTempRec.PhonePrivacy__c;
                 acc.SMS_Privacy__pc = eachCustomerTempRec.SMSPrivacy__c;
                 acc.PersonEmail=eachCustomerTempRec.Email__c;
                 acc.LastName=eachCustomerTempRec.LastName__c;
                 acc.FirstName=eachCustomerTempRec.FirstName__c;
                 System.debug('success'+eachCustomerTempRec.PrimaryStreet__c);
                 acc.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 acc.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 acc.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 acc.ShippingCountry='USA';
                 acc.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 acc.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                 acc.PersonBirthdate = eachCustomerTempRec.Birthdate__c;
                 acc.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 acc.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 acc.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                 acc.Relationship__c=eachCustomerTempRec.Relationship__c;
                 acc.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
                // acc.MeterRegisteredDate__c =eachCustomerTempRec.Meter_Most_Often_Used__c;
                 acc.PersonTitle=eachCustomerTempRec.Title__c;
                 acc.Suffix=eachCustomerTempRec.Suffix__c;
                 
// ************************** Manage Methods update with value in Account treatment method***************************************
               
                  String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
                  convertManagemethod = convertManagemethod.trim();
                  convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                   system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 system.debug('convertedMethods ==> ' +convertedMethods );
                  
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    
                  }
                 acc.TreatmentMethod__c = treatMethod ;
                 
// ************************** Manage Methods Ends here *********************************************  
               
                /* if(eachCustomerTempRec.ManageMethods__c == String.valueof(thirdNumber) )
                 {
                    acc.TreatmentMethod__c = 'Insulin Shots';
                 }
                 
                 List<string> ListManageMethods = convertManagemethod.split(';');
                 system.debug('ListManageMethods==>' +ListManageMethods );
                 for(Integer i=0;i<ListManageMethods.size();i++)
                 {
                 }*/
                 
                 
                  if(eachCustomerTempRec.Gender__c=='M')
                  {
                    acc.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    acc.Gender__pc='Female';
                  }
                 
                 /*Date myDate = System.today();
                 if(eachCustomerTempRec.Meter_Most_Often_Used__c=='Less then 1 yr')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-11);
                 }
                 else if(eachCustomerTempRec.Meter_Most_Often_Used__c=='1 yr')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-12);
                 }
                 else if(eachCustomerTempRec.Meter_Most_Often_Used__c=='2 yrs')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-24);
                 }
                 else if(eachCustomerTempRec.Meter_Most_Often_Used__c=='3 yrs')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-36);
                 }
                 else
                 {
                    acc.MeterRegisteredDate__c = myDate.addMonths(-37);
                 }
                 
                 //acc.Test_Frequency__pc=eachCustomerTempRec.Often_Testing__c;
                 
                
                 if(eachCustomerTempRec.Often_Testing__c=='4+ times per day')
                {
              
                acc.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.Often_Testing__c=='3 times per day')
                {
              
                acc.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.Often_Testing__c=='2 times per day')
                {
              
                acc.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.Often_Testing__c=='1 time per day')
                {
              
                acc.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.Often_Testing__c=='4-6 times per week')
                {
              
                acc.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.Often_Testing__c=='1-3 times per week')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.Often_Testing__c=='1-3 times per month')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.Often_Testing__c=='Less than once a month')
                {
              
                acc.Test_Frequency__pc=1;
                  }
                 */
                 
                 if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    acc.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    acc.Phone_Privacy__pc='LFS';
                  }
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    acc.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    acc.SMS_Privacy__pc='LFS';
                  }
                  
                  
                  insertAccounts.add(acc);
                 }
                 catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }
                   
                  deleteProcessedTempCustomers.add(eachCustomerTempRec);
               //}
               }
           
            
                  }
       
       if(!insertAccounts.isEmpty())
       {
           insert insertAccounts;
       }
       for(Account acc:insertAccounts)
       {
           system.debug('id is'+acc);
      
                 //create ProductsSelfReported
                      ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                    
                    newProductsSelfReported.Account__c = acc.id;
                   // newProductsSelfReported.Product__c =  acc.SerialNumber__c;
                    newProductsSelfReported.Product__c = '01tg00000043JjFAAU';
                    newProductsSelfReported.RegisteredDate__c =system.today();   
                    newProductsSelf.add(newProductsSelfReported);
                     
                    
      }
                  
            
             if(!newProductsSelf.isEmpty())
            {
           
            insert newProductsSelf;
            }
            
            if(!updateAccounts.isEmpty())
            {
              update updateAccounts;
            }
            
           
            
         }
   
   global void finish(Database.BatchableContext BC)
   {
           if(!deleteProcessedTempCustomers.isempty())
            {
              delete deleteProcessedTempCustomers;
            }
   }
  
}
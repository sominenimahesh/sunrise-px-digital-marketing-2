/** * 
* File Name: AddEmailOptInAccountsToCampaign
* Description : This is the service class to add Email Opt In Customers to Campaign
* Copyright : Johnson & Johnson
* * @author : Mahesh Somineni |msominen@its.jnj.com | mahesh.somineni@cognizant.com

* Modification Log =============================================================== 
*       Ver  |Date          |Author                 |Modification
*       1.0  |10/08/2015    |@msominen@its.jnj.com    |New Trigger created
* */ 
public class AddEmailOptInAccountsToCampaign{
    
    
  public void AddSMSContacts(List<Account> listAccounts,String EmailPrivacy,Set<Id> PrivacyIds)
  {
     String campaignId;
     String PreviousEmailPrivacy;
    
     System.debug('email privacy value '+EmailPrivacy);
     List<CampaignMember> cm = new List<CampaignMember>();   
        Map<Id, Campaign> cmMap = new Map<Id, Campaign>([Select Id,(Select ContactId From  CampaignMembers) From Campaign c Where Name = :EmailPrivacy]);
        List<Campaign> EmailCampaign = cmmap.values();                
        
         System.debug('campaign map '+cmMap);
          System.debug('contact list '+listAccounts);
          if(cmMap.size()>0)
          {
            System.debug('Opt if loop');
            campaignId = EmailCampaign[0].Id;
          }
          else
          {
            System.debug('Opt else loop');
            RecordType rt = [Select id,name from Recordtype where name='SMS' and sobjecttype='Campaign'];
            Campaign c = new Campaign();
            c.Name = EmailPrivacy;
            c.RecordTypeId = rt.id;
            c.IsActive=true;
            
            insert c;
            
            System.debug('campaign created'+c);
            
            campaignId = c.Id;
          }
          List<CampaignMemberStatus>  cmStatus= [Select id,CampaignId,HasResponded,Label,SortOrder from CampaignMemberStatus where Label='Not Started' and CampaignId=:campaignId];
          if(cmStatus.isEmpty())
          {
           CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId=campaignId, HasResponded=False, Label='Not Started', SortOrder=3);  
           try{
              insert cms2;
               }
           catch (DmlException ex) {
            System.Debug('Insert failed ' + ex);
             } 
          }
          list<campaignMember> campMembersList = [Select id,contactid from campaignMember where campaignid=:campaignId];
          Set<Id> contactIds = new Set<Id>();
          if(campMembersList!=null)
          {
            for(campaignMember campmem : campMembersList)
            {
              contactIds.add(campmem.contactid);
            }
          }
           if(EmailPrivacy == 'SMS OptIn')
            {
            PreviousEmailPrivacy = 'SMS OptOut';           
            }
        else if(EmailPrivacy == 'SMS OptOut')
            {
               PreviousEmailPrivacy = 'SMS OptIn';
            }
         
           System.debug('contact list '+PreviousEmailPrivacy);
          list<campaignMember> previouscampMembersList = [select Id from campaignmember where ContactId in:PrivacyIds and campaignId in (select Id from Campaign where name=:PreviousEmailPrivacy)];                
          System.debug('contact list '+previouscampMembersList);
         for(Account c : listAccounts) {
             CampaignMember newCM = new CampaignMember(
             CampaignId = campaignId,
             ContactId = c.PersonContactId,
             status = 'Not Started' );
                 if (!contactIds.contains(c.PersonContactId)) {
                     cm.add(newCM); 
                 }
         }
         
         

         try {
          System.debug('contact list '+cm);
             Database.insert(cm); 
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         } 
         try {
             if(previouscampMembersList!=null)
             {         
                 delete previouscampMembersList;
              }
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         }    
            
  }

  
  public void AddContacts(List<Account> listAccounts,String EmailPrivacy,Set<Id> PrivacyIds)
  {
     String campaignId;
     String PreviousEmailPrivacy;
    
     System.debug('email privacy value '+EmailPrivacy);
     List<CampaignMember> cm = new List<CampaignMember>();   
        Map<Id, Campaign> cmMap = new Map<Id, Campaign>([Select Id,(Select ContactId From  CampaignMembers) From Campaign c Where Name = :EmailPrivacy]);
        List<Campaign> EmailCampaign = cmmap.values();                
        
         System.debug('campaign map '+cmMap);
          System.debug('contact list '+listAccounts);
          if(cmMap.size()>0)
          {
            System.debug('Opt if loop');
            campaignId = EmailCampaign[0].Id;
          }
          else
          {
            System.debug('Opt else loop');
            RecordType rt = [Select id,name from Recordtype where name='Email' and sobjecttype='Campaign'];
            Campaign c = new Campaign();
            c.Name = EmailPrivacy;
              c.RecordTypeId = rt.id;
            c.IsActive=true;
            
            insert c;
            
            System.debug('campaign created'+c);
            
            campaignId = c.Id;
          }
          List<CampaignMemberStatus>  cmStatus= [Select id,CampaignId,HasResponded,Label,SortOrder from CampaignMemberStatus where Label='Not Started' and CampaignId=:campaignId];
          if(cmStatus.isEmpty())
          {
           CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId=campaignId, HasResponded=False, Label='Not Started', SortOrder=3);  
           try{
              insert cms2;
               }
           catch (DmlException ex) {
            System.Debug('Insert failed ' + ex);
             } 
          }
          list<campaignMember> campMembersList = [Select id,contactid from campaignMember where campaignid=:campaignId];
          Set<Id> contactIds = new Set<Id>();
          if(campMembersList!=null)
          {
            for(campaignMember campmem : campMembersList)
            {
              contactIds.add(campmem.contactid);
            }
          }
          if(EmailPrivacy == 'Email OptIn')
          {
            PreviousEmailPrivacy = 'Email OptOut';           
          }
        else if(EmailPrivacy == 'Email OptOut')
            {
               PreviousEmailPrivacy = 'Email OptIn';
            }
        
         
           System.debug('contact list '+PreviousEmailPrivacy);
          list<campaignMember> previouscampMembersList = [select Id from campaignmember where ContactId in:PrivacyIds and campaignId in (select Id from Campaign where name=:PreviousEmailPrivacy)];                
          System.debug('contact list '+previouscampMembersList);
         for(Account c : listAccounts) {
             CampaignMember newCM = new CampaignMember(
             CampaignId = campaignId,
             ContactId = c.PersonContactId,
             status = 'Not Started' );
                 if (!contactIds.contains(c.PersonContactId)) {
                     cm.add(newCM); 
                 }
         }
         
         

         try {
          System.debug('contact list '+cm);
             Database.insert(cm); 
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         } 
         try {
             if(previouscampMembersList!=null)
             {         
                 delete previouscampMembersList;
              }
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         }    
            
  }
  
  public void AddVoucherContacts(Map<Id,Account> mapVoucherAccounts,String voucherCampaignName)
  {
     String campaignId;
     List<Account> listAccounts = mapVoucherAccounts.values();
     Set<Id> VoucherAccountIds = mapVoucherAccounts.keySet();
    
     
     List<CampaignMember> cm = new List<CampaignMember>();   
        Map<Id, Campaign> cmMap = new Map<Id, Campaign>([Select Id,(Select ContactId From  CampaignMembers) From Campaign c Where Name = :voucherCampaignName]);
        List<Campaign> EmailCampaign = cmmap.values();                
        
         System.debug('campaign map '+cmMap);
          System.debug('contact list '+listAccounts);
          if(cmMap.size()>0)
          {
            
            campaignId = EmailCampaign[0].Id;
          }
          else
          {
            
            RecordType rt = [Select id,name from Recordtype where name='Email' and sobjecttype='Campaign'];
            Campaign c = new Campaign();
            c.Name = voucherCampaignName;
            c.IsActive=true;
            
            insert c;
            
            System.debug('campaign created'+c);
            
            campaignId = c.Id;
          }
          List<CampaignMemberStatus>  cmStatus= [Select id,CampaignId,HasResponded,Label,SortOrder from CampaignMemberStatus where Label='Not Started' and CampaignId=:campaignId];
          if(cmStatus.isEmpty())
          {
           CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId=campaignId, HasResponded=False, Label='Not Started', SortOrder=3);  
           try{
              insert cms2;
           }
           catch (DmlException ex) {
            System.Debug('Insert failed ' + ex);
         } 
          }
          list<campaignMember> campMembersList = [Select id,contactid from campaignMember where campaignid=:campaignId];
          list<campaignMember> updatecampMembersList = new list<campaignMember>();
          Map<Id,campaignMember> mapCampMembers = new Map<Id,campaignMember>();
          Set<Id> contactIds = new Set<Id>();
          if(campMembersList!=null)
          {
            for(campaignMember campmem : campMembersList)
            {
              contactIds.add(campmem.contactid);
              mapCampMembers.put(campmem.contactid,campmem);
            }
          } 
          
         for(Account c : listAccounts) {
             CampaignMember newCM = new CampaignMember(
             CampaignId = campaignId,
             ContactId = c.PersonContactId,
             ExpireDate__c = c.ExpireDate__c,
             VoucherGroupId__c = c.VoucherGroupId__c,
             VoucherNumber__c = c.VoucherNumber__c,
             status = 'Not Started' );
                 if (!contactIds.contains(c.PersonContactId)) {
                     cm.add(newCM); 
                 }
                 else
                 {
                   CampaignMember updateCM = mapCampMembers.get(c.PersonContactId);
                   updateCM.ExpireDate__c = c.ExpireDate__c;
                   updateCM.VoucherGroupId__c = c.VoucherGroupId__c;
                   updateCM.VoucherNumber__c = c.VoucherNumber__c;
                   updateCM.status = 'Not Started';
                   
                   updatecampMembersList.add(updateCM);
                 }
         }
         
         

         try {
          System.debug('contact list '+cm);
             Database.insert(cm); 
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         } 
         try {
          System.debug('contact list '+cm);
             Database.update(updatecampMembersList); 
         } catch (DmlException ex) {
            System.Debug('mahesh update failed ' + ex);
         } 
            
  }
  
  public void AddProductRegistration(List<Account> ProductRegisteredAccounts, String CampaignName)
  {
     
     String campaignId;
     
         
     List<CampaignMember> cm = new List<CampaignMember>();   
        Map<Id, Campaign> cmMap = new Map<Id, Campaign>([Select Id,(Select ContactId From  CampaignMembers) From Campaign  Where Name = :CampaignName]);
        List<Campaign> productRegistrationCampaign = cmmap.values();                
        
        
        
          if(cmMap.size()>0)
          {
            
            campaignId = productRegistrationCampaign[0].Id;
          }
          else
          {
            
            RecordType rt = [Select id,name from Recordtype where name='Email' and sobjecttype='Campaign'];
            Campaign c = new Campaign();
            c.Name = CampaignName;
            c.RecordTypeId = rt.id;
            c.IsActive=true;
            
            insert c;
            
            System.debug('campaign created'+c);
            
            campaignId = c.Id;
          }
           List<CampaignMemberStatus>  cmStatus= [Select id,CampaignId,HasResponded,Label,SortOrder from CampaignMemberStatus where Label='Not Started' and CampaignId=:campaignId];
          if(cmStatus.isEmpty())
          {
           CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId=campaignId, HasResponded=False, Label='Not Started', SortOrder=3);  
           try{
              insert cms2;
           }
           catch (DmlException ex) {
            System.Debug('Insert failed ' + ex);
         } 
          }
          
          list<campaignMember> campMembersList = [Select id,contactid from campaignMember where campaignid=:campaignId];
          list<campaignMember> updatecampMembersList = new list<campaignMember>();
          Map<Id,campaignMember> mapCampMembers = new Map<Id,campaignMember>();
          Set<Id> contactIds = new Set<Id>();
          if(campMembersList!=null)
          {
            for(campaignMember campmem : campMembersList)
            {
              contactIds.add(campmem.contactid);
              mapCampMembers.put(campmem.contactid,campmem);
            }
          } 
          
         for(Account c : ProductRegisteredAccounts) {
             CampaignMember newCM = new CampaignMember(
             CampaignId = campaignId,
             ContactId = c.PersonContactId,             
             status = 'Not Started' );
                 if (!contactIds.contains(c.PersonContactId)) {
                     cm.add(newCM); 
                 }
                 else
                 {
                   CampaignMember updateCM = mapCampMembers.get(c.PersonContactId);                  
                   updateCM.status = 'Not Started';
                   
                   updatecampMembersList.add(updateCM);
                 }
         }
         
         

         try {
          System.debug('contact list '+cm);
             Database.insert(cm); 
         } catch (DmlException ex) {
            System.Debug('mahesh Insert failed ' + ex);
         } 
         try {
          System.debug('contact list '+cm);
             Database.update(updatecampMembersList); 
         } catch (DmlException ex) {
            System.Debug('mahesh update failed ' + ex);
         } 
     
  }
}
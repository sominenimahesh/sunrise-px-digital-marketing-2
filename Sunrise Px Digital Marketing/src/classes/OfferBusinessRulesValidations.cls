/** * 
* File Name: OfferBusinessRulesValidations
* Description : Validation business logic class to chedk each business rules validation & eligibility.
* Copyright : Johnson & Johnson
* * @author : Manmohan Singh |mmo325@its.jnj.com | manmohan.singh1@cognizant.com

* Modification Log =============================================================== 
* 		Ver  |Date 			|Author 				|Modification
*		1.0  |23/07/2015	|@mmo325@its.jnj.com	|New Class created
* */ 
public class OfferBusinessRulesValidations {
    
    Public Static ProductsSelfReported__c createSelfRegisteredProducts(Account customer){//ProductsSelfReported__c
        try{
            ProductsSelfReported__c eachRecord = new ProductsSelfReported__c();
            eachRecord.Account__c = customer.id;
            eachRecord.Product__c = customer.OfferProductId__c;
            eachRecord.RegisteredDate__c = customer.MeterRegisteredDate__c;
            eachRecord.UniqueId__c = customer.id+':'+customer.OfferProductId__c;
            
            return eachRecord;
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.createSelfRegisteredProducts : '+ex.getMessage());
            System.debug('customer : '+customer);
        }
        return null;
    }
    
    //Self Reported Meter Check
    Public static boolean seftReportedMeter(Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(customer.OfferProductId__c !=null && (customer.OfferProductId__c).length()>0){
                    if(customer.OfferProductId__c != businessRuleValue){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    System.debug('Meter Mismatch');
                    System.debug('Customer Meter : '+customer.OfferProductId__c);
                    System.debug('Business Rule Meter : '+businessRuleValue);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.seftReportedMeterAge : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.OfferProductId__c);
        }
        return validationFlag;
    }
    //Allocate Coupon
    Public Static void  allocateTransaction(List<Account> updateAccountList,Account customer, Promotion_Transaction__c transctionNumber,Map<String,campaign> campaignMap){
        try{
            Account updateAcc = new Account();
            updateAcc.Voucher__c ='Completed';
            updateAcc.VoucherNumber__c = transctionNumber.VoucherNumber__c;
            updateAcc.VoucherGroupId__c = transctionNumber.GroupId__c;
            system.debug('Manmohan map '+campaignMap.get(customer.CampaignCode__c));
            updateAcc.ExpireDate__c = campaignMap.get(customer.CampaignCode__c).EndDate;
            updateAcc.VoucherStatusComments__c = 'Voucher has been indentified & populated.';
            updateAcc.id = customer.id;
            updateAccountList.add(updateAcc);
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.allocateTransaction : '+ex.getMessage());
            System.debug('Transction Number : '+transctionNumber);
            System.debug('customer : '+customer);
        }
        
    }
    //Transaction Not allowed.
    Public Static void  TransactionsAreNotAvailable(List<Account> updateAccountList,Account customer){
        try{
            Account updateAcc = new Account();
            updateAcc.Voucher__c ='Rejected';
            updateAcc.VoucherStatusComments__c = 'Voucher are not available., Please reachout to system admin for more details.';
            updateAcc.id = customer.id;
            updateAccountList.add(updateAcc);
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.TransactionsAreNotAvailable : '+ex.getMessage());
            System.debug('customer : '+customer);
        }
        
    }
    // Customer Type validation
    Public static boolean customerType(List<Account> updateAccountList,Account customer, String businessRuleValue,Map<ID, Schema.RecordTypeInfo> accountRecordTypeMap){
        boolean validationFlag = false;
        try{
            Account updateAcc = new Account();
            String value = accountRecordTypeMap.get(customer.RecordTypeId).getName();
            system.debug('Manmohan Account RecordType : '+value);
            String customerType = value.substringBefore('-').trim();
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(businessRuleValue.endsWithIgnoreCase('Patient;Non Patient')){
                    return validationFlag;
                }else{
                    if(!businessRuleValue.equalsIgnoreCase(customerType)){
                        validationFlag =  true;
                    }
                }
                
            }
            if(validationFlag){
                
                updateAcc.Voucher__c ='Rejected';
                updateAcc.VoucherStatusComments__c = 'Rejected bcause offers are allowed for '+businessRuleValue;
                updateAcc.id = customer.id;
                updateAccountList.add(updateAcc);
            }
            
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.customerType : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.ShippingState);
        }
        return validationFlag;
    }
    //Self Reported Meter Age.
    Public static boolean selfReportedMeterAge(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(customer.MeterRegisteredDate__c !=null){
                    if((system.today().addYears(-Integer.valueOf(businessRuleValue)))<(customer.MeterRegisteredDate__c)){validationFlag =  true;}else{
                        System.debug('customer.MeterRegisteredDate__c'+customer.MeterRegisteredDate__c);
                        System.debug('customer. new'+system.today().addYears(-Integer.valueOf(businessRuleValue)));
                    }
                }
                if(validationFlag){
                    Account updateAcc = new Account();
                    updateAcc.id = customer.id;
                    updateAcc.Voucher__c ='Rejected';
                    updateAcc.VoucherStatusComments__c = 'Rejected because Meter age is less than business rule.';
                    updateAccountList.add(updateAcc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.seftReportedMeterAge : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.MeterRegisteredDate__c);
        }
        return validationFlag;
    }
    
    // Date of birth validation
    Public static boolean dobBefore(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(customer.PersonBirthdate !=null){
                    system.debug('dobBefoe :'+system.today().addYears(-Integer.valueOf(businessRuleValue)));
                    if((system.today().addYears(-Integer.valueOf(businessRuleValue)))<(customer.PersonBirthdate)){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account acc = new Account();
                    acc.Voucher__c ='Rejected';
                    
                    acc.VoucherStatusComments__c = 'Rejected because Age is less than business rule.';
                    acc.id = customer.id;
                    updateAccountList.add(acc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.dobBefore : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.ShippingState);
        }
        return validationFlag;
    }
    // Date of birth validation -dobAfter
    Public static boolean dobAfter(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(customer.PersonBirthdate !=null){
                    if((system.today().addYears(-Integer.valueOf(businessRuleValue)))>(customer.PersonBirthdate)){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account acc = new Account();
                    acc.Voucher__c ='Rejected';
                    acc.VoucherStatusComments__c = 'Rejected because Age is greater than business rule.';
                    acc.id = customer.id;
                    updateAccountList.add(acc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.dobBefore : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.ShippingState);
        }
        return validationFlag;
    }
    
    // Country Validaiton 
    Public static boolean countryValidaiton(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            
            if(businessRuleValue !=null && businessRuleValue.length()>0  && !businessRuleValue.equalsIgnoreCase('null')){
                if(customer.ShippingCountry !=null && (customer.ShippingCountry).length()>0 && !(customer.ShippingCountry).equalsIgnoreCase('null')){
                    if(!businessRuleValue.contains(customer.ShippingCountry)){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account acc = new Account();
                    acc.Voucher__c ='Rejected';
                    acc.VoucherStatusComments__c = 'Rejected bcause of Country validaiton failed';
                    acc.id = customer.id;
                    updateAccountList.add(acc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.countryValidaiton : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.ShippingCountry);
        }
        return validationFlag;
    }
    // State Validaiton 
    Public static boolean stateValidaiton(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            if(businessRuleValue !=null && businessRuleValue.length()>0 && !businessRuleValue.equalsIgnoreCase('null')){
                if(customer.ShippingState !=null && (customer.ShippingState).length()>0 && !(customer.ShippingState).equalsIgnoreCase('null')){
                    if(!businessRuleValue.contains(customer.ShippingState)){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account acc = new Account();
                    acc.Voucher__c ='Rejected';
                    acc.VoucherStatusComments__c = 'Rejected bcause of State validaiton failed.';
                    acc.id = customer.id;
                    updateAccountList.add(acc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.stateValidaiton : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.ShippingState);
        }
        return validationFlag;
    }
    // Treatment Validaiton
    Public static boolean treatmentMethodValidaiton(List<Account> updateAccountList,Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            if(businessRuleValue !=null && businessRuleValue.length()>0 && !businessRuleValue.equalsIgnoreCase('null')){
                if(customer.TreatmentMethod__c !=null && (customer.TreatmentMethod__c).length()>0 && !(customer.TreatmentMethod__c).equalsIgnoreCase('null')){
                    //if(!businessRuleValue.contains(customer.TreatmentMethod__c)){validationFlag =  true;}
                    String tmCus = customer.TreatmentMethod__c;
                    Map<String,String> businessRuleTreatmentMethod = new Map<String,String>();
                    if(businessRuleValue.contains(';')){
                        String[] treatmentArr = businessRuleValue.split(';');
                        for(String eachTmCus : treatmentArr){
                            businessRuleTreatmentMethod.put(eachTmCus,eachTmCus);
                        }
                    }else{
                         businessRuleTreatmentMethod.put(businessRuleValue,businessRuleValue);
                    }
                    if(tmCus.contains(';')){
                        String[] treatmentArr = tmCus.split(';');
                        for(String eachTmCus : treatmentArr){
                            validationFlag =  true;
                            if(businessRuleTreatmentMethod.containsKey(eachTmCus)){
                                validationFlag =  false;
                                break;
                            }
                        }
                    }else{
                        if(!businessRuleTreatmentMethod.containsKey(customer.TreatmentMethod__c)){validationFlag =  true;}
                    }
                    system.debug('businessRuleTreatmentMethod : '+businessRuleTreatmentMethod);
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account acc = new Account();
                    acc.Voucher__c ='Rejected';
                    acc.VoucherStatusComments__c = 'Rejected bcause of treatment method validaiton failed';
                    acc.id = customer.id;
                    updateAccountList.add(acc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.treatmentMethodValidaiton : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.TreatmentMethod__c);
        }
        return validationFlag;
    }
    
    
    
    
    
    
    
    Public static boolean acquisitionChannelValidaiton(Account customer, String businessRuleValue){
        boolean validationFlag = false;
        try{
            if(businessRuleValue !=null && businessRuleValue.length()>0){
                if(customer.Acquisition_Channel__pc !=null && (customer.Acquisition_Channel__pc).length()>0){
                    if(!businessRuleValue.contains(customer.Acquisition_Channel__pc)){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    customer.Voucher__c ='Rejected';
                    customer.VoucherStatusComments__c = 'Rejected bcause of Acquisition Channel validaiton failed';
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.acquisitionChannelValidaiton : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.Acquisition_Channel__pc);
        }
        System.debug('validationFlag : '+validationFlag);
        return validationFlag;
    }
    
    //Test Frequency Per Week
    
    Public static boolean testFrequencyPerWeek(List<Account> updateAccountList,Account customer, Decimal businessRuleValue){
        boolean validationFlag = false;
        try{
            
            if(businessRuleValue !=null){
                if(customer.Test_Frequency__pc !=null ){
                    if(businessRuleValue !=customer.Test_Frequency__pc){validationFlag =  true;}
                }else{validationFlag =  true;}
                if(validationFlag){
                    Account updateAcc = new Account();
                    updateAcc.id = customer.id;
                    updateAcc.Voucher__c ='Rejected';
                    updateAcc.VoucherStatusComments__c = 'Rejected because Test Frequency business rule failed.';
                    updateAccountList.add(updateAcc);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.testFrequencyPerWeek : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.Test_Frequency__pc);
        }
        return validationFlag;
    }
    
    // State Validaiton 
    
    
    
    
    Public Static void  TransactionsAreNotAvailable(Account customer){
        try{
            customer.Voucher__c ='Rejected';
            customer.VoucherStatusComments__c = 'Voucher are not available., Please reachout to system admin for more details.';
            
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.TransactionsAreNotAvailable : '+ex.getMessage());
            System.debug('customer : '+customer);
        }
        
    }
    Public Static void  TransactionsAreNotAvailableMessage(List<Account> updateAccountList,Account customer,String message){
        try{
            Account updateCustomer = new Account();
            updateCustomer.Voucher__c ='Rejected';
            updateCustomer.VoucherStatusComments__c = 'Rejected :'+message;
            updateCustomer.id = customer.id;
            updateAccountList.add(updateCustomer);
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulesValidations.TransactionsAreNotAvailableMessage : '+ex.getMessage());
            System.debug('customer : '+customer);
            System.debug('message : '+message);
        }
        
    }
    // Meter Age Validation + offer count
    Public static boolean offerCountByDurationPerHouseHold(List<Account> updateAccountList,Account customer, String Days, String allowableProductCount){
        boolean validationFlag = false;
        try{
            List<Promotion_Transaction__c> returnVouchers = OfferBusinessRulesSOQL.getCouponsPerHouseHold(
                customer.ShippingStreet,
                customer.ShippingCity,
                customer.ShippingState,
                customer.ShippingPostalCode,
                customer.ShippingCountry,
                customer.CampaignCode__c
            );
            System.debug('Household returnVouchers'+returnVouchers);
            integer count = 0;
            if(Days!=null && Days.length()>0 && !days.equalsIgnoreCase('null')){
                for(Promotion_Transaction__c each: returnVouchers){
                    if((each.ReceivedDate__c)>(system.today().addDays(-Integer.valueOf(Days)))){
                        count++;
                    }
                }
            }else{
                count = returnVouchers.size();
            }
            
            System.debug(allowableProductCount+'Household'+count);
            if(allowableProductCount!=null && allowableProductCount.length()>0 && !allowableProductCount.equalsIgnoreCase('null')){
                if(count>=Integer.valueof(allowableProductCount))
                    validationFlag= true;
            }
            
            if(validationFlag){
                Account acc = new Account();
                acc.Voucher__c ='Rejected';
                acc.VoucherStatusComments__c = 'Rejected : maximum hosehold offer limit exceed.';
                acc.id = customer.Id;
                updateAccountList.add(acc);
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.offerCountByDurationPerHouseHold : '+ex.getMessage());
            
        }
        return validationFlag;
    } 
    // Meter Age Validation + offer count
    Public static boolean offerCountByDuration(List<Account> updateAccountList,Account customer, String Days, String allowableProductCount){
        boolean validationFlag = false;
        try{
            List<Promotion_Transaction__c> returnVouchers = OfferBusinessRulesSOQL.getCouponsPerPerson(customer.id,customer.CampaignCode__c);
            
            System.debug('returnVouchers'+returnVouchers);
            integer count = 0;
            if(Days!=null && Days.length()>0 && !days.equalsIgnoreCase('null')){
                for(Promotion_Transaction__c each: returnVouchers){
                    if((each.ReceivedDate__c)>(system.today().addDays(-Integer.valueOf(Days)))){
                        count++;
                    }
                }
            }else{
                count = returnVouchers.size();
            }
            
            System.debug(allowableProductCount+'returnVouchers'+count);
            if(allowableProductCount!=null && allowableProductCount.length()>0 && !allowableProductCount.equalsIgnoreCase('null')){
                if(count>=Integer.valueof(allowableProductCount))
                    validationFlag= true;
            }else{
                
            }
            
            if(validationFlag){
                Account acc = new Account();
                acc.Voucher__c ='Rejected';
                acc.VoucherStatusComments__c = 'Rejected : maximum offer limit exceed.';
                acc.id = customer.Id;
                updateAccountList.add(acc);
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.seftReportedMeterAge : '+ex.getMessage());
            
        }
        return validationFlag;
    } 
    public static Map<String,List<Device__c>> getDevicesByCustomer(Set<String> listOfCustomers){
        Map<String,List<Device__c>> customerDeviceListMap = new Map<String,List<Device__c>>();
        List<Device__c> listOfDevice = OfferBusinessRulesSOQL.getDevicesByCustomer(listOfCustomers);
        for(Device__c eachDevice : listOfDevice){
            if(customerDeviceListMap.containsKey(eachDevice.Patient__c)){
                List<Device__c> eachList = customerDeviceListMap.get(eachDevice.Patient__c);
                eachList.add(eachDevice);
                customerDeviceListMap.put(eachDevice.Patient__c, eachList);
            }else{
                List<Device__c> eachList = new List<Device__C>();
                eachList.add(eachDevice);
                customerDeviceListMap.put(eachDevice.Patient__c, eachList);
            }
        }
        return customerDeviceListMap;
    }
    //Self Reported Meter Check
    Public static boolean registeredMeterAndAge(List<Account> updateAccountList,Account customer, String businessRuleValue,List<Device__c> listOfDevices){
        boolean validationFlag = false;
        try{
            system.debug('businessRuleValue : '+businessRuleValue);
            system.debug('customer.OfferProductId__c : '+customer.OfferProductId__c);
             if(businessRuleValue !=null && businessRuleValue.length()>0){
                boolean productMatchFlag = true;
                 if(customer.OfferProductId__c !=null && (customer.OfferProductId__c).length()>0){
                    system.debug('listOfDevices : '+listOfDevices);
                    for(Device__c eachDevice : listOfDevices){
                        if(eachDevice.Product__c==customer.OfferProductId__c){
                            productMatchFlag =false;
                            system.debug('eachDevice.Registered_Date__c : '+eachDevice.Registered_Date__c);
                            if((system.today().addYears(-Integer.valueOf(businessRuleValue)))<(eachDevice.Registered_Date__c))
                            {
                                validationFlag =  true;
                                Account acc = new Account();
                				acc.Voucher__c ='Rejected';
                				acc.VoucherStatusComments__c = 'Rejected : Registered Meter age is less then rule.';
                				acc.id = customer.Id;
                				updateAccountList.add(acc);
                                
                            }else{
                                System.debug('customer.MeterRegisteredDate__c'+customer.MeterRegisteredDate__c);
                                System.debug('customer. new'+system.today().addYears(-Integer.valueOf(businessRuleValue)));
                            }
                        }
                    }
                    if(productMatchFlag)
                    validationFlag =  true;
                }else{validationFlag =  true;}
                if(validationFlag){
                    System.debug('Meter Mismatch');
                    System.debug('Registerd Meter not found: '+customer.OfferProductId__c);
                    System.debug('Business Rule Meter : '+businessRuleValue);
                }
            }
        }Catch(exception ex){
            System.debug('Exception in OfferBusinessRulessslidations.seftReportedMeterAge : '+ex.getMessage());
            System.debug('businessRuleValue : '+businessRuleValue);
            System.debug('customerValue : '+customer.OfferProductId__c);
        }
        return validationFlag;
    }
    
}
/** * 
* File Name: OfferBusinessRulesSOQL
* Description : THis class is to keep the Query Scripts
* Copyright : Johnson & Johnson
* * @author : Manmohan Singh |mmo325@its.jnj.com | manmohan.singh1@cognizant.com

* Modification Log =============================================================== 
* 		Ver  |Date 			|Author 				|Modification
*		1.0  |23/07/2015	|@mmo325@its.jnj.com	|New Class created
* */ 

public class OfferBusinessRulesSOQL {
    
    public static List<campaign> getCampaingsWithBusinessRules(Set<String> listOfCampaignCodes){
        return(
            [Select Id,Campaign_Code__c,EndDate,
             (Select Id,IsActiveRule__c,
              AcquisitionChannel__c,
              AgeGreaterThan__c,
              AgeLessthan__c,
              AllowedProductCount__c,
              Campaign__c,
              CustomerType__c,
              Legacy_Record_Id__c,
              NonEmptyFields__c,
              ProductUS__c,
              RegisteredYear__c,
              ProductUniqueId__c,
              ProgramAwareness__c,
              ShippingCountry__c,
              ShippingState__c,
              TreatmentMethod__c,FrequencyPerWeek__c,IsHousehold__c,
              ProductType__c,Days__c
              from Promotion_Rules__r) 
             	from campaign 
             	where 
             	Campaign_Code__c =:listOfCampaignCodes
          ]);
    }
//
    public static List<Promotion_Item__c> getSourceGroupMappings(Set<String> sourceList,Set<String> campList){
        String sourceStr = '';
        String campStr = '';
        if(sourceList.size() > 0){
                
                Integer i = 0;

                for(String agId : sourceList){
                    if(i==sourceList.size()-1){
                        sourceStr += '\''+agId+'\'';    
                    }else{
                        sourceStr += '\''+agId+'\',';
                    }
                    i++;
                }
       } 
        if(campList.size() > 0){
                
                Integer i = 0;

                for(String agId : campList){
                    if(i==campList.size()-1){
                        campStr += '\''+agId+'\'';    
                    }else{
                        campStr += '\''+agId+'\',';
                    }
                    i++;
                }
       } 
        String query ='Select Id,Name,Group__c,Sources__c,Campaign__r.Campaign_Code__c from Promotion_Item__c where Sources__c INCLUDES ('+sourceStr+') and Campaign__r.Campaign_Code__c IN ('+campStr+')';
		return database.query(query);
    }
    public static List<Promotion_Transaction__c> getAvailableTransactions(String CampaignCode, String groupName){
        return(
            [Select Id,
             Name,
             Account__c,
             Promotion_Item__c,
             Campaign__r.Campaign_Code__c,
             SourceName__c,GroupId__c,VoucherNumber__c,ReceivedDate__c,CampaignCode__c,Group__c,
             Promotion_Item__r.Name
             from 
             Promotion_Transaction__c 
             where
             Status__c='Not Started' and 
             Campaign__r.Campaign_Code__c =:campaignCode and 
             Group__c =:groupName limit 200]
        );
    }
    //Select id,Name,Account__c,CampaignCode__c from Promotion_Transaction__c where  Account__r.ShippingCity = 'FREMONT'
 public static List<Promotion_Transaction__c> getCouponsPerHouseHold(
     String Street,
     String city,String state,String postal,String country,String campaignCode
 ){
        return(
            [Select Id,
             Name,
             Account__c,
             Promotion_Item__c,
             Campaign__r.Campaign_Code__c,
             SourceName__c,GroupId__c,VoucherNumber__c,ReceivedDate__c,CampaignCode__c,Group__c,
             Promotion_Item__r.Name
             from 
             Promotion_Transaction__c 
             where 
             Account__r.ShippingStreet =: Street 
             AND Account__r.ShippingCity =: city 
             AND Account__r.ShippingState =: state 
             AND Account__r.ShippingPostalCode =: postal
             AND Account__r.ShippingCountry = :country
             AND CampaignCode__c =:campaignCode
            ]
        );
    }
    //Select id,Name,Account__c,CampaignCode__c from Promotion_Transaction__c where  Account__r.ShippingCity = 'FREMONT'
 public static List<Promotion_Transaction__c> getCouponsPerPerson(
     String customerId,String campaignCode
 ){
        return(
            [Select Id,
             Name,
             Account__c,
             Promotion_Item__c,
             Campaign__r.Campaign_Code__c,
             SourceName__c,GroupId__c,VoucherNumber__c,ReceivedDate__c,CampaignCode__c,Group__c,
             Promotion_Item__r.Name
             from 
             Promotion_Transaction__c 
             where 
             Account__c = :customerId
             AND CampaignCode__c =:campaignCode
            ]
        );
    }
     public static List<Device__c> getDevicesByCustomer(Set<String> listOfCustomers){
        return(
            [Select id,Name,Patient__c,Product__c,Registered_Date__c from Device__c where Patient__c=:listOfCustomers
              ]);
    }
}
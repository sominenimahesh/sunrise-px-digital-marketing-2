global class CustomDCL implements Database.Batchable<sObject>,Database.Stateful 
{
    global final String query;
    global final String DCL;
    
    List<CustomerTempTable__c> deleteProcessedTempCustomers = new List<CustomerTempTable__c>();
       
    global CustomDCL()
    {  
      
       
      DCL = 'DCL';
     query='select id,FirstName__c,LastName__c,Diabetes__c,SerialNumber__c,Title__c,Suffix__c,MeterMostOftenUsed__c,Relationship__c,OftenTesting__c,RegisteredDate__c,ManageMethods__c,Mobile__c,HomePhone__c,Age__c, Email__c,DirectMailPrivacy__c,EmailPrivacy__c,PhonePrivacy__c,SMSPrivacy__c, Address__c, Country__c, PrimaryCity__c, PrimaryStateProvince__c, PrimaryStreet__c, PrimaryZipPostalCode__c,Gender__c,TypeOfFile__c  From CustomerTempTable__c where TypeOfFile__c=:DCL';
       
       
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      return Database.getQueryLocator(query);
    }

   global void execute(Database.BatchableContext BC, List<Sobject> scope)
   {
       
        List<CustomerTempTable__c> ListofTempCustomer = (List<CustomerTempTable__c>)scope;
         RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='Patient - US' ]; 
        RecordType rp = [select id,Name from RecordType where SobjectType='Account' and Name='Non Patient - US']; 
          String Fname ;
          String UFname ;
           
         
          String Lname;
          String ULname;
         List<Account> insertAccounts1 =new List<Account>(); 
         List<Account> insertAccounts =new List<Account>();       
         List<Account> updateAccounts =new List<Account>();
          List<productsSelfReported__c> updateselfproduct=new List<productsSelfReported__c>();
         List<ProductsSelfReported__c> newProductsSelf =new List<ProductsSelfReported__c>();
             List<ProductsSelfReported__c> newProductsSelf1 =new List<ProductsSelfReported__c>();
     
     
      List<ProductsSelfReported__c> Oldselfproduct = [Select Account__c,id,RegisteredDate__c,Product__c from ProductsSelfReported__c LIMIT 50000];   
         Map<Id,List<ProductsSelfReported__c>> OldselfreportedMap = new Map<id,List<ProductsSelfReported__c>>();
            

        for(ProductsSelfReported__c de : Oldselfproduct) {
            if(OldselfreportedMap.containsKey(de.Account__c)) {
            List<ProductsSelfReported__c> oself= OldselfreportedMap.get(de.Account__c);
             oself.add(de);
            OldselfreportedMap.put(de.Account__c, oself);
             } else {
             OldselfreportedMap.put(de.Account__c, new List<ProductsSelfReported__c> { de });
            }
        }
        
       for(CustomerTempTable__c eachCustomerTempRec : ListofTempCustomer)
        {
      
             Fname = eachCustomerTempRec.FirstName__c;
             UFname = Fname.toUpperCase();
             
             system.debug('UFname ==>'+UFname );
             
             Lname = eachCustomerTempRec.LastName__c;
             ULname = Lname.toUpperCase();
             system.debug('address ==>'+eachCustomerTempRec.Address__c);
             
              List<Account> ListAccount =[Select id,FirstName,LastName,PersonHomePhone,PersonEmail
            From Account where FirstName =:UFname AND LastName =:ULname AND PersonEmail=:eachCustomerTempRec.Email__c];  
            
             
             system.debug('ListAccount==>'+ListAccount);
              CustomerTempTable__c c = new CustomerTempTable__c();
              
              if(listAccount.size()> 0)
               {
                 Boolean ProductsSelfReported = false;
                 System.debug('entered if loop');
                  Account ac = new Account();
                
                 ac.Id = ListAccount[0].id;
                 ac.Direct_Mail_Privacy__pc =eachCustomerTempRec.DirectMailPrivacy__c;
                 ac.Email_Privacy__pc =eachCustomerTempRec.EmailPrivacy__c;
                 ac.Phone_Privacy__pc =eachCustomerTempRec.PhonePrivacy__c;
                 ac.SMS_Privacy__pc =eachCustomerTempRec.SMSPrivacy__c;
                 ac.PersonEmail=eachCustomerTempRec.Email__c;
                 ac.LastName=eachCustomerTempRec.LastName__c;
                 ac.FirstName=eachCustomerTempRec.FirstName__c;
                 ac.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 ac.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 ac.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 ac.ShippingCountry='USA';
                 ac.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 ac.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                // ac.MeterRegisteredDate__c =eachCustomerTempRec.RegisteredDate__c;
                 ac.Age__c = eachCustomerTempRec.Age__c;
                 ac.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 ac.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 ac.Relationship__c=eachCustomerTempRec.Relationship__c;
                 ac.PersonTitle=eachCustomerTempRec.Title__c;
                 ac.Suffix=eachCustomerTempRec.Suffix__c;
                // ac.RecordTypeId = rt.id;
                // ac.RecordTypeId = rp.id;
                 ac.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
                 ac.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                 ac.Email_Permission_Source__pc='DCL';
                 ac.Direct_Mail_Permission_Source__pc='DCL';
                 ac.SMS_Permission_Source__pc='DCL';
                 ac.Phone_Permission_Source__pc='DCL';
                 ac.TreatmentMethod__c = eachCustomerTempRec.ManageMethods__c;
       //**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
           /*String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
            convertManagemethod = convertManagemethod.trim();
                convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                  
                   system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 ac.TreatmentMethod__c = treatMethod ;*/
                 
//********************************** Managemethods ends here ***************************************************************************          
               
               Date myDate = System.today();
                 if(eachCustomerTempRec.MeterMostOftenUsed__c=='Less than 1 year')
                 {
                     
                     ac.MeterRegisteredDate__c = myDate.addMonths(-11);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='1 yr')
                 {
                     
                     ac.MeterRegisteredDate__c = myDate.addMonths(-12);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='2 years')
                 {
                     
                     ac.MeterRegisteredDate__c = myDate.addMonths(-24);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='3+ years')
                 {
                     
                     ac.MeterRegisteredDate__c = myDate.addMonths(-36);
                 }
                 else
                 {
                    ac.MeterRegisteredDate__c = myDate.addMonths(-37);
                 }
                 
                 //acc.Test_Frequency__pc=eachCustomerTempRec.OftenTesting__c;
                 
                
                 if(eachCustomerTempRec.OftenTesting__c=='4+ times per week')
                {
              
                ac.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.OftenTesting__c=='3 times per day')
                {
              
                ac.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.OftenTesting__c=='2 times per day')
                {
              
                ac.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.OftenTesting__c=='1 time per day')
                {
              
                ac.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.OftenTesting__c=='4-6 times per week')
                {
              
                ac.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.OftenTesting__c=='1-3 times per week')
                {
              
                ac.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.OftenTesting__c=='1-3 times per month')
                {
              
                ac.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.OftenTesting__c=='Less than once a month')
                {
              
                ac.Test_Frequency__pc=1;
                  }
                 
                 
               
                  
                   if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    ac.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    ac.Phone_Privacy__pc='LFS';
                  }
                  
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    ac.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    ac.SMS_Privacy__pc='LFS';
                  }
                     if(eachCustomerTempRec.Gender__c=='M')
                  {
                    ac.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    ac.Gender__pc='Female';
                  }
                  
                   updateAccounts.add(ac);
                
                     
                  
                  deleteProcessedTempCustomers.add(eachCustomerTempRec);
                  
                 system.debug('Vicky==>'+updateAccounts);
                  List<productsSelfReported__c> oproduct = OldselfreportedMap.get(ListAccount[0].id);
                   if(oproduct!=null)
                    {
                   for(productsSelfReported__c d: oproduct)
                   {
                       if(d.Product__c==eachCustomerTempRec.SerialNumber__c )
                       {
                        d.RegisteredDate__c=ac.MeterRegisteredDate__c;
                           ProductsSelfReported = true;
                           updateselfproduct.add(d);
                            
                       }
                         
                   }
                    
                    
                   }
                   if(ProductsSelfReported==false)
                   {
                   
                   
                  
                 
                   ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                    
                   newProductsSelfReported.Account__c = ListAccount[0].id;
                   newProductsSelfReported.Product__c = eachCustomerTempRec.SerialNumber__c;
                   newProductsSelfReported.RegisteredDate__c = ac.MeterRegisteredDate__c;                
                   newProductsSelf.add(newProductsSelfReported);
                   
                   
                   
                 
                   
                    // }
                     //}
                    }
                   
                   
                    
                 }                
               else
                  {
                  
                      System.debug('entered else loop');
 //********************************************************************************patient US *****************************************************************                                 
                   //if(listAccount.isEmpty) 
              // {
              
              
                      try{ Account acc = new Account();
                      if(eachCustomerTempRec.Diabetes__c=='Y'){
                 
                 acc.Direct_Mail_Privacy__pc = eachCustomerTempRec.DirectMailPrivacy__c;
                 acc.Email_Privacy__pc = eachCustomerTempRec.EmailPrivacy__c;
                 acc.Phone_Privacy__pc = eachCustomerTempRec.PhonePrivacy__c;
                 acc.SMS_Privacy__pc = eachCustomerTempRec.SMSPrivacy__c;
                 acc.PersonEmail=eachCustomerTempRec.Email__c;
                 acc.LastName=eachCustomerTempRec.LastName__c;
                 acc.FirstName=eachCustomerTempRec.FirstName__c;
                 System.debug('success'+eachCustomerTempRec.PrimaryStreet__c);
                 acc.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 acc.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 acc.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 acc.ShippingCountry='USA';
                 acc.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 acc.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                 acc.Age__c = eachCustomerTempRec.Age__c;
                 acc.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 acc.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 acc.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                 acc.Relationship__c=eachCustomerTempRec.Relationship__c;
                 acc.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
               
                 acc.PersonTitle=eachCustomerTempRec.Title__c;
                 acc.Suffix=eachCustomerTempRec.Suffix__c;
                 acc.RecordTypeId = rt.id;
                 acc.TreatmentMethod__c = eachCustomerTempRec.ManageMethods__c;
                 acc.Email_Permission_Source__pc='DCL';
                 acc.Direct_Mail_Permission_Source__pc='DCL';
                 acc.SMS_Permission_Source__pc='DCL';
                 acc.Phone_Permission_Source__pc='DCL';
   //**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
          /* String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
                  convertManagemethod = convertManagemethod.trim();
                convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                  
                 //  system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 
   

                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 acc.TreatmentMethod__c = treatMethod;*/
                 
//********************************** Managemethods ends here ***************************************************************************              
                 Date myDate = System.today();
                 if(eachCustomerTempRec.MeterMostOftenUsed__c=='Less than 1 year')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-11);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='1 yr')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-12);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='2 years')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-24);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='3+ years')
                 {
                     
                     acc.MeterRegisteredDate__c = myDate.addMonths(-36);
                 }
                 else
                 {
                    acc.MeterRegisteredDate__c = myDate.addMonths(-37);
                 }
                 
                 //acc.Test_Frequency__pc=eachCustomerTempRec.OftenTesting__c;
                 
                
                 if(eachCustomerTempRec.OftenTesting__c=='4+ times per week')
                {
              
                acc.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.OftenTesting__c=='3 times per day')
                {
              
                acc.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.OftenTesting__c=='2 times per day')
                {
              
                acc.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.OftenTesting__c=='1 time per day')
                {
              
                acc.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.OftenTesting__c=='4-6 times per week')
                {
              
                acc.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.OftenTesting__c=='1-3 times per week')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.OftenTesting__c=='1-3 times per month')
                {
              
                acc.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.OftenTesting__c=='Less than once a month')
                {
              
                acc.Test_Frequency__pc=1;
                  }
                     if(eachCustomerTempRec.Gender__c=='M')
                  {
                    acc.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    acc.Gender__pc='Female';
                  }
                 
                 
                 if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    acc.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    acc.Phone_Privacy__pc='LFS';
                  }
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    acc.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    acc.SMS_Privacy__pc='LFS';
                  }
                 
                  
                  insertAccounts.add(acc);
                }
                 }
                 catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }
                      
//*************************************************************Patient US*************************************************************************************
//***********************************************************************Non patient US********************************************************************                   
                       
                       
                    // System.debug('Y'+eachCustomerTempRec.Diabetes__c);            
                      try{ Account acc1 = new Account();
                        if(eachCustomerTempRec.Diabetes__c=='N'){
                      
                      acc1.RecordTypeId = rp.id;
                      
                   acc1.Direct_Mail_Privacy__pc = eachCustomerTempRec.DirectMailPrivacy__c;
                 acc1.Email_Privacy__pc = eachCustomerTempRec.EmailPrivacy__c;
                 acc1.Phone_Privacy__pc = eachCustomerTempRec.PhonePrivacy__c;
                 acc1.SMS_Privacy__pc = eachCustomerTempRec.SMSPrivacy__c;
                 acc1.PersonEmail=eachCustomerTempRec.Email__c;
                 acc1.LastName=eachCustomerTempRec.LastName__c;
                 acc1.FirstName=eachCustomerTempRec.FirstName__c;
                 System.debug('success'+eachCustomerTempRec.PrimaryStreet__c);
                 acc1.ShippingStreet=eachCustomerTempRec.PrimaryStreet__c;
                 acc1.ShippingState=eachCustomerTempRec.PrimaryStateProvince__c;
                 acc1.ShippingPostalCode=eachCustomerTempRec.PrimaryZipPostalCode__c;
                 acc1.ShippingCountry='USA';
                 acc1.ShippingCity=eachCustomerTempRec.PrimaryCity__c;
                 acc1.SerialNumber__c = eachCustomerTempRec.SerialNumber__c;
                 acc1.Age__c = eachCustomerTempRec.Age__c;
                 acc1.PersonMobilePhone = eachCustomerTempRec.Mobile__c;
                 acc1.PersonHomePhone = eachCustomerTempRec.HomePhone__c;
                 acc1.TypeOfFile__c=eachCustomerTempRec.TypeOfFile__c;
                 acc1.Relationship__c=eachCustomerTempRec.Relationship__c;
                 acc1.TreatmentMethod__c=eachCustomerTempRec.ManageMethods__c;
               
                 acc1.PersonTitle=eachCustomerTempRec.Title__c;
                 acc1.Suffix=eachCustomerTempRec.Suffix__c;
                 acc1.Email_Permission_Source__pc='DCL';
                 acc1.Direct_Mail_Permission_Source__pc='DCL';
                 acc1.SMS_Permission_Source__pc='DCL';
                 acc1.Phone_Permission_Source__pc='DCL';
                 acc1.TreatmentMethod__c = eachCustomerTempRec.ManageMethods__c;
     //**************** Manage Methods >> IT will convert value to treatment method as multipicklist *****************************************
          
          /* String convertManagemethod = eachCustomerTempRec.ManageMethods__c;
                  convertManagemethod = convertManagemethod.trim();
                convertManagemethod = convertManagemethod.replaceAll('(\\s+)','');
                  
                 //  system.debug('convertManagemethod ==> ' +convertManagemethod );
                  
                 String[] convertedMethods = convertManagemethod.split(';');
                 
   

                 system.debug('convertedMethods ==> ' +convertedMethods );
                  //final string NUMBER_OF_HOURS_IN_A_DAY = 'Insulin';
                  //constant ss = insulin;
                   //final Integer thirdNumber = 3;
                  // final Integer twonumber = 2;
                  // final Integer onenumber = 1;
                  Map<Integer,String> mapManageMethods = new Map<Integer,String>();
                  mapManageMethods.put(1,'Diet');
                  mapManageMethods.put(2,'Pills');
                  mapManageMethods.put(3,'Insulin Shots');
                  mapManageMethods.put(4,'Insulin Pump');
                  mapManageMethods.put(5,'Exercise');
                  mapManageMethods.put(6,'Other');
                  mapManageMethods.put(7,'Shots other than insulin');
                  
                  string treatMethod='';
                  for(String s:convertedMethods)
                  {
                    String s1 = mapManageMethods.get(Integer.valueof(s));
                    system.debug('s1  ==> ' +s1 );
                    
                    //acc.TreatmentMethod__c+=s1+'+;';
                    treatMethod+= s1+';';
                    //treatMethod = s1;
                  }
                 acc1.TreatmentMethod__c = treatMethod;*/
                 
//********************************** Managemethods ends here ***************************************************************************          
                 Date myDate = System.today();
                 if(eachCustomerTempRec.MeterMostOftenUsed__c=='Less than 1 year')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate.addMonths(-11);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='1 yr')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate.addMonths(-12);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='2 years')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate.addMonths(-24);
                 }
                 else if(eachCustomerTempRec.MeterMostOftenUsed__c=='3+ years')
                 {
                     
                     acc1.MeterRegisteredDate__c = myDate.addMonths(-36);
                 }
                 else
                 {
                    acc1.MeterRegisteredDate__c = myDate.addMonths(-37);
                 }
                 
                 //acc1.Test_Frequency__pc=eachCustomerTempRec.OftenTesting__c;
                 
                
                 if(eachCustomerTempRec.OftenTesting__c=='4+ times per week')
                {
              
                acc1.Test_Frequency__pc=28;
                  }                
                  if(eachCustomerTempRec.OftenTesting__c=='3 times per day')
                {
              
                acc1.Test_Frequency__pc=21;
                  }
              
                    if(eachCustomerTempRec.OftenTesting__c=='2 times per day')
                {
              
                acc1.Test_Frequency__pc=14;
                  }
                 
                     if(eachCustomerTempRec.OftenTesting__c=='1 time per day')
                {
              
                acc1.Test_Frequency__pc=7;
                  }
                      if(eachCustomerTempRec.OftenTesting__c=='4-6 times per week')
                {
              
                acc1.Test_Frequency__pc=5;
                  }
                        if(eachCustomerTempRec.OftenTesting__c=='1-3 times per week')
                {
              
                acc1.Test_Frequency__pc=2;
                  }
                           if(eachCustomerTempRec.OftenTesting__c=='1-3 times per month')
                {
              
                acc1.Test_Frequency__pc=2;
                  }
                  
                   if(eachCustomerTempRec.OftenTesting__c=='Less than once a month')
                {
              
                acc1.Test_Frequency__pc=1;
                  }
                 
                 
                 if(eachCustomerTempRec.EmailPrivacy__c=='Y')
                  {
                    acc1.Email_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.PhonePrivacy__c=='Y')
                  {
                    acc1.Phone_Privacy__pc='LFS';
                  }
                   if(eachCustomerTempRec.DirectMailPrivacy__c=='Y')
                  {
                    acc1.Direct_Mail_Privacy__pc='LFS';
                  }
                    if(eachCustomerTempRec.SMSPrivacy__c=='Y')
                  {
                    acc1.SMS_Privacy__pc='LFS';
                  }
                  
                     if(eachCustomerTempRec.Gender__c=='M')
                  {
                    acc1.Gender__pc='Male';
                  }
                    if(eachCustomerTempRec.Gender__c=='F')
                  {
                    acc1.Gender__pc='Female';
                  }
                 
                  
                  insertAccounts1.add(acc1);
                  } 
                }
                 catch (Exception e)
                 {
                     system.debug('erro Message'+e.getMessage());
                 }
                     
                        
                      
                       deleteProcessedTempCustomers.add(eachCustomerTempRec);
               //}
               }
           
            
                  }
       
       if(!insertAccounts1.isEmpty())
       {
           insert insertAccounts1;
       }
       
       if(!insertAccounts.isEmpty())
       {
           insert insertAccounts;
       }  
//**********************************************************************Non-patient us END**********************************************************       
      for(Account acc1:insertAccounts1)
       {
           system.debug('id is'+acc1);
      
                 //create ProductsSelfReported
                      ProductsSelfReported__c newProductsSelfReported1 = new ProductsSelfReported__c();
                    
                    newProductsSelfReported1.Account__c = acc1.id;
                    newProductsSelfReported1.Product__c =  acc1.SerialNumber__c;
                    newProductsSelfReported1.RegisteredDate__c =acc1.MeterRegisteredDate__c;   
                    newProductsSelf1.add(newProductsSelfReported1);
                     
                    
      }
//*********************************************************selfreported for non patient US End**********************************************      
//*************************************************************selfreported for patient Us***************************************************   
    for(Account acc:insertAccounts)
       {
           system.debug('id is'+acc);
      
                 //create ProductsSelfReported
                      ProductsSelfReported__c newProductsSelfReported = new ProductsSelfReported__c();
                    
                    newProductsSelfReported.Account__c = acc.id;
                    newProductsSelfReported.Product__c =  acc.SerialNumber__c;
                    newProductsSelfReported.RegisteredDate__c =acc.MeterRegisteredDate__c;   
                    newProductsSelf.add(newProductsSelfReported);
                     
                    
      }
 //****************************************************************selfreported for patient us end here**********************************************     
          if(!newProductsSelf.isEmpty())
            {
           
            insert newProductsSelf;
            }
            if(!newProductsSelf1.isEmpty())
            {
           
            insert newProductsSelf1;
            }
           
          if(!updateAccounts.isEmpty())
            {
              update updateAccounts;
            }
           if(!updateselfproduct.isEmpty())
            {
              update updateselfproduct;
            }  
          
            
         } 
   
   global void finish(Database.BatchableContext BC)
   {
           if(!deleteProcessedTempCustomers.isempty())
            {
              delete deleteProcessedTempCustomers;
            }
   }
  
}
<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GenderUpdate</fullName>
        <description>Update Gender F to Female,  M to Male  and Blank to Unspecified</description>
        <field>PatientGender__c</field>
        <formula>IF( ISPICKVAL(PatientGenderPickllist__c, &apos;F&apos;),&quot;Female&quot;, IF( ISPICKVAL(PatientGenderPickllist__c, &apos;M&apos;),&quot;Male&quot;, IF( ISPICKVAL(PatientGenderPickllist__c, &apos;Blank&apos;),&quot;Unspecified&quot;,Null)))</formula>
        <name>GenderUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MailOrderIndicatorValues</fullName>
        <description>01=Retail, 05=Mail Order</description>
        <field>MailOrderIndicator__c</field>
        <formula>IF(ISPICKVAL(MailOrderIndicatorPickList__c, &apos;1&apos;) , &quot;Revisit&quot;, IF(ISPICKVAL(MailOrderIndicatorPickList__c, &apos;5&apos;) ,&quot;Mail Order&quot;, IF(ISPICKVAL(MailOrderIndicatorPickList__c, &apos;None&apos;) , &quot;&quot;,Null)))</formula>
        <name>MailOrderIndicatorValues</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OtherCoverCodeDescrption</fullName>
        <description>0=Not Specified 
1=No Other Coverage Identified 
2=Other coverage exists-payment collected (Obsolete Dec 2011) 
3=Other coverage exists-claim not covered 
4=Other coverage exists-payment not collected (Obsolete Dec 2011) 
8=Claim is billing for Copay</description>
        <field>OtherCoverageCode__c</field>
        <formula>IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;0&apos;) , &quot;Not Specified&quot;, 
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;1&apos;) ,&quot;No Other Coverage Identified &quot;, 
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;2&apos;) ,&quot;No Other Coverage Identified &quot;,
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;3&apos;) ,&quot;Other coverage exists-claim not covered  &quot;,
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;4&apos;) ,&quot;Other coverage exists-payment not collected &quot;,
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;8&apos;) ,&quot;Claim is billing for Copay &quot;,
IF(ISPICKVAL(OtherCoverageCodePickList__c, &apos;None&apos;) , &quot;&quot;,Null)))))))</formula>
        <name>OtherCoverCodeDescrption</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>claimTypePlusOrginalNew</fullName>
        <field>ClaimType__c</field>
        <formula>IF(ISPICKVAL(ClaimTypePicklist__c, &apos;+&apos;) , &quot;original claim &quot;, IF(ISPICKVAL(ClaimTypePicklist__c, &apos;R&apos;) , &quot;Reversal/Adjustment &quot;, IF(ISPICKVAL(ClaimTypePicklist__c, &apos;None&apos;) , &quot;&quot;,Null)))</formula>
        <name>claim Type Plus Orginal New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Claim Type</fullName>
        <actions>
            <name>claimTypePlusOrginalNew</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Claim Type are updated to - original claim 
R - Reversal/Adjustment</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MultiOrderIndicator</fullName>
        <actions>
            <name>MailOrderIndicatorValues</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>01=Retail, 05=Mail Order</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OtherCoverageUpdated</fullName>
        <actions>
            <name>OtherCoverCodeDescrption</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>0=Not Specified 
1=No Other Coverage Identified 
2=Other coverage exists-payment collected (Obsolete Dec 2011) 
3=Other coverage exists-claim not covered 
4=Other coverage exists-payment not collected (Obsolete Dec 2011) 
8=Claim is billing for Copay</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PatientGender</fullName>
        <actions>
            <name>GenderUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Patient Gender to change value from F to Female and M to Male</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

<!--
============================================================================================== 
PAGE FOR SHOWING HELP INTERACTION STATISTICS IN SUMMARY FORM.
REQUIRES EITHER A HELPED ELEMENT ID (ElemID) AND PAGE LAYOUT IDENTIFIER, I.E:

    [SF base URL]/Apex/IHStats?ElemID=[helped element id]&HPL=[Page identifier]
    
OR A HELP TOPIC ID, I.E:

    [SF base URL]/Apex/IHStats?HTID=[Help topic ID] 

WHERE ELEMENT ID HAS BEEN PASSED AND MORE THAN 1 MATCH EXISTS (THERE BEING MULTIPLE ELEMENTS
WITH THE SAME IDENTIFIER BUT DIFFERENT DATA FILTERS) OPTIONALLY SPECIFY THE DESIRED ELEMENT RECORD ID
WITHIN THE SET OF MATCHING ELEMENTS, I.E:

    [SF base URL]/Apex/IHStats?ElemID=[helped element id]&HPL=[Page identifier]&Id=[record Id]

Martin Little for Improved Apps
September 2012
Copyright (c.) Improved Apps Limited 2012. All Rights Reserved.
============================================================================================== 
 -->
 
 <apex:page standardStylesheets="False" showHeader="False" sidebar="False" 
                title="Improved Help: Topic Statistics"
                controller="iahelp.ControllerStats"
                action="{!getStats}">

	<head>
		<title>Improved Help: Topic Statistics</title>
		<meta name='Description' content='Improved Help Copyright (c.) Improved Apps Limited 2012. All Rights Reserved.' />		
	    <apex:stylesheet value="{!BrandCSS}" />
	    <apex:stylesheet value="{!URLFOR($Resource.iahelp__IHResources, '/css/help_main.css')}" />
	    
	    <style>
	    	.IHSelectorBullet:link	{text-decoration: none;}
	    </style>	
	</head>    

    <body class='Dialogue'>     
        
        <div id='CalloutStats' class='Callout'>
            <apex:form >
                <div class='CalloutContent Panel' style="width: 98%;">                  
                    
                    <div style="width:50%; float: left;">
                        <div class='IH-H3'>
                            <apex:outputText value="Statistics for '{!ElemName}':" escape="true"></apex:outputText>&nbsp;&nbsp;
                            
                            	                            
							<apex:outputPanel rendered="{!HEs != null}">
		                            
	                            <apex:repeat var="HE" value="{!HEs}">
	                            	<span style="font-size: 14px;">
		                            	<apex:outputLink styleClass="IHSelectorBullet" rendered="{!IF(HEs.size > 1, true, false)}" 
		                            						title="{!IF(HE.Id != RequestedElemId, 'See statistics for matching element: ' + HE.Name, '')}"
		                            						value="{!$Page.iahelp__IHStats + '?ElemId=' + HE.iahelp__Identifier__c + '&HPL=' + HPL + '&Id=' + HE.Id}" >
		                            						
		                            		<apex:outputText escape="false" value="{!IF(HE.Id != RequestedElemId, '&#9675;', '')}" />
		                            		
		                            	</apex:outputLink>
		                            	
		                            	<apex:outputText escape="false" rendered="{!IF(HE.Id == RequestedElemId && HEs.size > 1, true, false)}" value="&#9679;" />

	                            	</span>
	                            </apex:repeat>

							</apex:outputPanel>	                                                       
                        </div>                        
        
                        <div class="InlineStatsNarrative">
                            <div class="InlineStatsNarrativeLine">
                                <b>• Callout views: </b>
                                <apex:outputText value="{!NumSummaryViews}" escape="true"></apex:outputText>
                            </div>
    
                            <div class="InlineStatsNarrativeLine">
                                <b>• Full Topic views : </b>
                                <apex:outputText value="{!NumFullViews}" escape="true"></apex:outputText>                               
                            </div>
    
                            <div class="InlineStatsNarrativeLine">
                                <b>• User Comments: </b>
                                <apex:outputText value="{!NumComments}" escape="true"></apex:outputText>
                            </div>
                            
                            <div class="InlineStatsNarrativeLine">
                                <span class='IHError'>{!IssuesWarning}</span>
                            </div>
                                                                            
                        </div>                      
                        
                    </div>
                    <div style="width:50%; float: right;">
                              
                        <div class='IAIHTip InlineStatsTip' style="float: right; position: relative; top: 45px;">
                            <a href="{!MoreLink}" title="See details of these interactions" target="_Stats">
                                Open a detailed report for this Help Topic...
                            </a>
                        </div>
        
                        <div class="InlineStatsChart" style="float: right;">
                            <apex:chart data="{!GraphInfo}" height="65px" width="140px" legend="false" animate="0" colorSet="#A3C0DA,#004080,#ff8000,">
                                <apex:pieSeries labelField="itemTip" dataField="itemValue"/>
                            </apex:chart>
                        </div>

                    </div>                        
                  
                </div>
           
            </apex:form>
        </div>                
    </body>

</apex:page>